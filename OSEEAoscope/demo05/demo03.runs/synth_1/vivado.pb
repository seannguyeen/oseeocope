
}
Command: %s
53*	vivadotcl2L
8synth_design -top OSEEAoscope_TOP -part xc7a100tcsg324-12default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7a100t2default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7a100t2default:defaultZ17-349h px� 
�
)literal value truncated to fit in %s bits2292*oasys2
102default:default2q
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/vertScale_conversion.v2default:default2
262default:default8@Z8-2292h px� 
�
)literal value truncated to fit in %s bits2292*oasys2
102default:default2q
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/vertScale_conversion.v2default:default2
412default:default8@Z8-2292h px� 
�
%s*synth2�
xStarting RTL Elaboration : Time (s): cpu = 00:00:02 ; elapsed = 00:00:02 . Memory (MB): peak = 460.152 ; gain = 114.180
2default:defaulth px� 
�
synthesizing module '%s'%s4497*oasys2#
OSEEAoscope_TOP2default:default2
 2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
32default:default8@Z8-6157h px� 
b
%s
*synth2J
6	Parameter ADDRESS_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter REAL_DISPLAY_WIDTH bound to: 1688 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter REAL_DISPLAY_HEIGHT bound to: 1066 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter SAMPLE_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter TOGGLE_CHANNELS_STATE_BITS bound to: 2 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_X_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_Y_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter RGB_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter SCALE_EXPONENT_BITS bound to: 4 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SCALE_FACTOR_BITS bound to: 10 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter SELECT_CHARACTER_BITS bound to: 7 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter GREEN bound to: 12'b000011000000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter LIGHT_PURPLE bound to: 12'b111101101111 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2
	clk_wiz_02default:default2
 2default:default2�
zC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.runs/synth_1/.Xil/Vivado-13668-DESKTOP-PNS2FA1/realtime/clk_wiz_0_stub.v2default:default2
52default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
	clk_wiz_02default:default2
 2default:default2
12default:default2
12default:default2�
zC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.runs/synth_1/.Xil/Vivado-13668-DESKTOP-PNS2FA1/realtime/clk_wiz_0_stub.v2default:default2
52default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
debounce2default:default2
 2default:default2e
OC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/debounce.v2default:default2
32default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter DELAY bound to: 270000 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
debounce2default:default2
 2default:default2
22default:default2
12default:default2e
OC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/debounce.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
one_shot2default:default2
 2default:default2e
OC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/one_shot.v2default:default2
32default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
one_shot2default:default2
 2default:default2
32default:default2
12default:default2e
OC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/one_shot.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
settings2default:default2
 2default:default2e
OC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/settings.v2default:default2
232default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter DATA_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SAMPLE_PERIOD_BITS bound to: 6 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SCALE_FACTOR_SIZE bound to: 10 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter TRIGGER_THRESHOLD_ADJUST bound to: 96 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter SCALE_EXPONENT_BITS bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_Y_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter INCREASE_PIXEL_COUNT bound to: 1000000 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter COUNT_BITS bound to: 20 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
settings2default:default2
 2default:default2
42default:default2
12default:default2e
OC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/settings.v2default:default2
232default:default8@Z8-6155h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
52default:default2
sw2default:default2
162default:default2
settings2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1072default:default8@Z8-689h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
SETTINGS2default:default2
settings2default:default2
232default:default2
212default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1062default:default8@Z8-350h px� 
�
synthesizing module '%s'%s4497*oasys2(
vertScale_conversion2default:default2
 2default:default2q
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/vertScale_conversion.v2default:default2
32default:default8@Z8-6157h px� 
g
%s
*synth2O
;	Parameter SCALE_FACTOR_SIZE bound to: 10 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter SCALE_EXPONENT_BITS bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
-case statement is not full and has no default155*oasys2q
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/vertScale_conversion.v2default:default2
152default:default8@Z8-155h px� 
�
-case statement is not full and has no default155*oasys2q
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/vertScale_conversion.v2default:default2
302default:default8@Z8-155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2(
vertScale_conversion2default:default2
 2default:default2
52default:default2
12default:default2q
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/vertScale_conversion.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
xadc2default:default2
 2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/xadc.v2default:default2
52default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
XADC2default:default2
 2default:default2K
5C:/Vivado/Vivado/2018.2/scripts/rt/data/unisim_comp.v2default:default2
525332default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter INIT_40 bound to: 16'b1000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_41 bound to: 16'b0100000100001111 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_42 bound to: 16'b0000010000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_43 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_44 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_45 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_46 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_47 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_48 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_49 bound to: 16'b0000000000001000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_4A bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_4B bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_4C bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_4D bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_4E bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_4F bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_50 bound to: 16'b1011010111101101 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_51 bound to: 16'b0101011111100100 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_52 bound to: 16'b1010000101000111 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_53 bound to: 16'b1100101000110011 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_54 bound to: 16'b1010100100111010 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_55 bound to: 16'b0101001011000110 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_56 bound to: 16'b1001010101010101 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_57 bound to: 16'b1010111001001110 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_58 bound to: 16'b0101100110011001 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_59 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_5A bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_5B bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_5C bound to: 16'b0101000100010001 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_5D bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_5E bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter INIT_5F bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter IS_CONVSTCLK_INVERTED bound to: 1'b0 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter IS_DCLK_INVERTED bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter SIM_DEVICE bound to: 7SERIES - type: string 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter SIM_MONITOR_FILE bound to: design.txt - type: string 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
XADC2default:default2
 2default:default2
62default:default2
12default:default2K
5C:/Vivado/Vivado/2018.2/scripts/rt/data/unisim_comp.v2default:default2
525332default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
xadc2default:default2
 2default:default2
72default:default2
12default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/xadc.v2default:default2
52default:default8@Z8-6155h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
82default:default2
daddr_in2default:default2
72default:default2
xadc2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-689h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
XADC2default:default2
xadc2default:default2
192default:default2
182default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-350h px� 
�
synthesizing module '%s'%s4497*oasys2"
toggle_channel2default:default2
 2default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/toggle_channel.v2default:default2
32default:default8@Z8-6157h px� 
a
%s
*synth2I
5	Parameter SAMPLE_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
M
%s
*synth25
!	Parameter IDLE bound to: 2'b00 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter READ_CHANNEL_1 bound to: 2'b01 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter READ_CHANNEL_2 bound to: 2'b10 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter INVALID_STATE bound to: 2'b11 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter STATE_BITS bound to: 2 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter DRP_ADDRESS_BITS bound to: 7 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter CHANNEL_1_ADDRESS bound to: 7'b0010011 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter CHANNEL_2_ADDRESS bound to: 7'b0011011 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2"
toggle_channel2default:default2
 2default:default2
82default:default2
12default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/toggle_channel.v2default:default2
32default:default8@Z8-6155h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
82default:default2

DRPAddress2default:default2
72default:default2"
toggle_channel2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1782default:default8@Z8-689h px� 
�
synthesizing module '%s'%s4497*oasys2"
adc_controller2default:default2
 2default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/adc_controller.v2default:default2
32default:default8@Z8-6157h px� 
]
%s
*synth2E
1	Parameter IO_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter INPUT_OFFSET bound to: 2048 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter NUMERATOR_SCALE_FACTOR bound to: 11'sb01111101000 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter DENOMINATOR_SCALE_FACTOR bound to: 1024 - type: integer 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter DENOMINATOR_RIGHT_SHIFT bound to: 10 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter NUMERATOR_SCALED_DATA_BITS bound to: 22 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2"
adc_controller2default:default2
 2default:default2
92default:default2
12default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/adc_controller.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2#
low_pass_filter2default:default2
 2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
32default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter DATA_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2#
low_pass_filter2default:default2
 2default:default2
102default:default2
12default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
32default:default8@Z8-6155h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
142default:default2"
estimatedSlope2default:default2
122default:default2#
low_pass_filter2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2132default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
142default:default2"
estimatedSlope2default:default2
122default:default2#
low_pass_filter2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2242default:default8@Z8-689h px� 
�
synthesizing module '%s'%s4497*oasys2!
buffer_select2default:default2
 2default:default2j
TC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer_select.v2default:default2
32default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2!
buffer_select2default:default2
 2default:default2
112default:default2
12default:default2j
TC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer_select.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
buffer2default:default2
 2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
32default:default8@Z8-6157h px� 
a
%s
*synth2I
5	Parameter LOG_SAMPLES bound to: 12 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter SAMPLE_SIZE bound to: 12 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2!
blk_mem_gen_02default:default2
 2default:default2�
~C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.runs/synth_1/.Xil/Vivado-13668-DESKTOP-PNS2FA1/realtime/blk_mem_gen_0_stub.v2default:default2
62default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2!
blk_mem_gen_02default:default2
 2default:default2
122default:default2
12default:default2�
~C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.runs/synth_1/.Xil/Vivado-13668-DESKTOP-PNS2FA1/realtime/blk_mem_gen_0_stub.v2default:default2
62default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
buffer2default:default2
 2default:default2
132default:default2
12default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
32default:default8@Z8-6155h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
142default:default2
dataIn2default:default2
122default:default2
buffer2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2552default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
142default:default2
dataIn2default:default2
122default:default2
buffer2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2832default:default8@Z8-689h px� 
�
synthesizing module '%s'%s4497*oasys2"
channel_toggle2default:default2
 2default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/channel_toggle.v2default:default2
32default:default8@Z8-6157h px� 
a
%s
*synth2I
5	Parameter SAMPLE_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SCALE_FACTOR_BITS bound to: 10 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter SCALE_EXPONENT_BITS bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2"
channel_toggle2default:default2
 2default:default2
142default:default2
12default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/channel_toggle.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2

sw_trigger2default:default2
 2default:default2g
QC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/sw_trigger.v2default:default2
32default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter DATA_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter TRIGGER_HOLDOFF bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

sw_trigger2default:default2
 2default:default2
152default:default2
12default:default2g
QC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/sw_trigger.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2"
measure_signal2default:default2
 2default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/measure_signal.v2default:default2
32default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter DATA_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter MAX_MIN_RESET_DATA_CYCLES bound to: 2000000 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2"
measure_signal2default:default2
 2default:default2
162default:default2
12default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/measure_signal.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2!
xvga1280_10242default:default2
 2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/xvga.v2default:default2
32default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2!
xvga1280_10242default:default2
 2default:default2
172default:default2
12default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/xvga.v2default:default2
32default:default8@Z8-6155h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
122default:default2
displayX2default:default2
112default:default2!
xvga1280_10242default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
3552default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
122default:default2
displayY2default:default2
112default:default2!
xvga1280_10242default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
3562default:default8@Z8-689h px� 
�
synthesizing module '%s'%s4497*oasys2
Grid2default:default2
 2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/Grid.v2default:default2
32default:default8@Z8-6157h px� 
d
%s
*synth2L
8	Parameter DISPLAY_X_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_Y_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter GRID_COLOR bound to: 12'b110011001100 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter COLOR_PIXELS bound to: 100 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter ADDITIONAL_LINE_PIXELS bound to: 0 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter ADDITIONAL_ZERO_LINE_PIXELS bound to: 1 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
Grid2default:default2
 2default:default2
182default:default2
12default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/Grid.v2default:default2
32default:default8@Z8-6155h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
122default:default2
pixel2default:default2
1002default:default2
Grid2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
3802default:default8@Z8-689h px� 
�
synthesizing module '%s'%s4497*oasys2
	draw_wave2default:default2
 2default:default2f
PC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/draw_wave.v2default:default2
32default:default8@Z8-6157h px� 
b
%s
*synth2J
6	Parameter DATA_IN_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SCALE_FACTOR_BITS bound to: 10 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_X_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_Y_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter RGB_COLOR bound to: 12'b111111110000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter RGB_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter DISPLAY_WIDTH bound to: 1280 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter DISPLAY_HEIGHT bound to: 1024 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter REAL_DISPLAY_WIDTH bound to: 1688 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter REAL_DISPLAY_HEIGHT bound to: 1066 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter HEIGHT_ZERO_PIXEL bound to: 512 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter ADDITIONAL_WAVE_PIXELS bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ADDRESS_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
	draw_wave2default:default2
 2default:default2
192default:default2
12default:default2f
PC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/draw_wave.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2-
draw_wave__parameterized02default:default2
 2default:default2f
PC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/draw_wave.v2default:default2
32default:default8@Z8-6157h px� 
b
%s
*synth2J
6	Parameter DATA_IN_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SCALE_FACTOR_BITS bound to: 10 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_X_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_Y_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter RGB_COLOR bound to: 12'b000011111111 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter RGB_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter DISPLAY_WIDTH bound to: 1280 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter DISPLAY_HEIGHT bound to: 1024 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter REAL_DISPLAY_WIDTH bound to: 1688 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter REAL_DISPLAY_HEIGHT bound to: 1066 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter HEIGHT_ZERO_PIXEL bound to: 512 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter ADDITIONAL_WAVE_PIXELS bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ADDRESS_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2-
draw_wave__parameterized02default:default2
 2default:default2
192default:default2
12default:default2f
PC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/draw_wave.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2(
draw_horizontal_line2default:default2
 2default:default2q
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/draw_horizontal_line.v2default:default2
32default:default8@Z8-6157h px� 
b
%s
*synth2J
6	Parameter DATA_IN_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_X_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_Y_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter RGB_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter RGB_COLOR bound to: 12'b111100000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter DISPLAY_WIDTH bound to: 1280 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter DISPLAY_HEIGHT bound to: 1024 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter HEIGHT_ZERO_PIXEL bound to: 512 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter ADDITIONAL_LINE_PIXELS bound to: 1 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2(
draw_horizontal_line2default:default2
 2default:default2
202default:default2
12default:default2q
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/draw_horizontal_line.v2default:default2
32default:default8@Z8-6155h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
322default:default2
level2default:default2
122default:default2(
draw_horizontal_line2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
4582default:default8@Z8-689h px� 
�
synthesizing module '%s'%s4497*oasys28
$draw_horizontal_line__parameterized02default:default2
 2default:default2q
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/draw_horizontal_line.v2default:default2
32default:default8@Z8-6157h px� 
b
%s
*synth2J
6	Parameter DATA_IN_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_X_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_Y_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter RGB_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter RGB_COLOR bound to: 12'b000011000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter DISPLAY_WIDTH bound to: 1280 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter DISPLAY_HEIGHT bound to: 1024 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter HEIGHT_ZERO_PIXEL bound to: 512 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter ADDITIONAL_LINE_PIXELS bound to: 1'b0 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys28
$draw_horizontal_line__parameterized02default:default2
 2default:default2
202default:default2
12default:default2q
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/draw_horizontal_line.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
cursor_text2default:default2
 2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/cursor_text.v2default:default2
32default:default8@Z8-6157h px� 
d
%s
*synth2L
8	Parameter DISPLAY_Y_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter SCALE_EXPONENT_BITS bound to: 4 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SCALE_FACTOR_SIZE bound to: 10 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter SELECT_CHARACTER_BITS bound to: 7 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter VOLTAGE_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter DIGIT_BITS bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2%
cursor_to_voltage2default:default2
 2default:default2n
XC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/cursor_to_voltage.v2default:default2
32default:default8@Z8-6157h px� 
b
%s
*synth2J
6	Parameter VOLTAGE_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_Y_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter SCALE_EXPONENT_BITS bound to: 4 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter PIXELS_TIMES_250 bound to: 18 - type: integer 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter PIXELS_TIMES_250_SCALED bound to: 21 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SCALE_FACTOR_SIZE bound to: 10 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2%
cursor_to_voltage2default:default2
 2default:default2
212default:default2
12default:default2n
XC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/cursor_to_voltage.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2"
binary2decimal2default:default2
 2default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/binary2decimal.v2default:default2
32default:default8@Z8-6157h px� 
U
%s
*synth2=
)	Parameter COMPUTING_BCD bound to: 1'b1 
2default:defaulth p
x
� 
L
%s
*synth24
 	Parameter IDLE bound to: 1'b0 
2default:defaulth p
x
� 
�
default block is never used226*oasys2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/binary2decimal.v2default:default2
182default:default8@Z8-226h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2"
binary2decimal2default:default2
 2default:default2
222default:default2
12default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/binary2decimal.v2default:default2
32default:default8@Z8-6155h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
122default:default2
data2default:default2
102default:default2"
binary2decimal2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/cursor_text.v2default:default2
362default:default8@Z8-689h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
cursor_text2default:default2
 2default:default2
232default:default2
12default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/cursor_text.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
get_timeDiv2default:default2
 2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/get_timeDiv.v2default:default2
162default:default8@Z8-6157h px� 
g
%s
*synth2O
;	Parameter SAMPLE_PERIOD_BITS bound to: 6 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter SAMPLE_PERIOD_BITS_PLUS_ONE bound to: 7 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter TIME_PER_DIVISION_BITS bound to: 10 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
get_timeDiv2default:default2
 2default:default2
242default:default2
12default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/get_timeDiv.v2default:default2
162default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
Text2default:default2
 2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
32default:default8@Z8-6157h px� 
d
%s
*synth2L
8	Parameter DISPLAY_X_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_Y_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHARACTER_WIDTH bound to: 18 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter CHARACTER_WIDTH_BITS bound to: 5 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter CHARACTER_HEIGHT bound to: 32 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter CHARACTER_HEIGHT_BITS bound to: 5 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter SELECT_CHARACTER_BITS bound to: 7 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter BRAM_ADDRESS_BITS bound to: 17 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CHARACTER_COLOR bound to: 12'b000011110000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter CURSOR_1_COLOR bound to: 12'b000011110000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter COLOR_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2

mark_debug2default:default2
true2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
812default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2

mark_debug2default:default2
true2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
842default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2

mark_debug2default:default2
true2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
872default:default8@Z8-5534h px� 
�
synthesizing module '%s'%s4497*oasys2!
CharactersROM2default:default2
 2default:default2�
~C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.runs/synth_1/.Xil/Vivado-13668-DESKTOP-PNS2FA1/realtime/CharactersROM_stub.v2default:default2
62default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2!
CharactersROM2default:default2
 2default:default2
252default:default2
12default:default2�
~C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.runs/synth_1/.Xil/Vivado-13668-DESKTOP-PNS2FA1/realtime/CharactersROM_stub.v2default:default2
62default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
Text2default:default2
 2default:default2
262default:default2
12default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
lookup_text2default:default2
 2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
32default:default8@Z8-6157h px� 
d
%s
*synth2L
8	Parameter DISPLAY_X_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter DISPLAY_Y_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CHARACTER_WIDTH bound to: 18 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter CHARACTER_WIDTH_BITS bound to: 5 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter CHARACTER_HEIGHT bound to: 32 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter CHARACTER_HEIGHT_BITS bound to: 5 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter SELECT_CHARACTER_BITS bound to: 7 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter BRAM_ADDRESS_BITS bound to: 17 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CHARACTER_COLOR bound to: 12'b000011110000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter CURSOR_1_COLOR bound to: 12'b111100001111 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter LIGHT_PURPLE bound to: 12'b111101101111 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter COLOR_BITS bound to: 12 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
lookup_text2default:default2
 2default:default2
272default:default2
12default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
32default:default8@Z8-6155h px� 
�
0Net %s in module/entity %s does not have driver.3422*oasys2,
curveAddressOut2Channel12default:default2#
OSEEAoscope_TOP2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2512default:default8@Z8-3848h px� 
�
0Net %s in module/entity %s does not have driver.3422*oasys2,
curveAddressOut2Channel22default:default2#
OSEEAoscope_TOP2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2792default:default8@Z8-3848h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2#
OSEEAoscope_TOP2default:default2
 2default:default2
282default:default2
12default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
32default:default8@Z8-6155h px� 
�
!design %s has unconnected port %s3331*oasys2%
cursor_to_voltage2default:default2
scale[9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
cursor_to_voltage2default:default2
scale[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
cursor_to_voltage2default:default2
scale[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
cursor_to_voltage2default:default2
scale[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
cursor_to_voltage2default:default2
scale[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
cursor_to_voltage2default:default2
scale[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
cursor_to_voltage2default:default2
scale[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
cursor_to_voltage2default:default2
scale[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
cursor_to_voltage2default:default2
scale[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
cursor_to_voltage2default:default2
scale[0]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
settings2default:default2
sw[15]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
settings2default:default2
sw[14]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
settings2default:default2
sw[13]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
settings2default:default2
sw[12]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
settings2default:default2
sw[11]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
settings2default:default2
sw[10]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
settings2default:default2
sw[7]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
settings2default:default2
sw[6]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
settings2default:default2
sw[5]2default:defaultZ8-3331h px� 
�
%s*synth2�
xFinished RTL Elaboration : Time (s): cpu = 00:00:03 ; elapsed = 00:00:03 . Memory (MB): peak = 537.758 ; gain = 191.785
2default:defaulth px� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
	di_in[15]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
	di_in[14]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
	di_in[13]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
	di_in[12]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
	di_in[11]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
	di_in[10]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
di_in[9]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
di_in[8]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
di_in[7]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
di_in[6]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
di_in[5]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
di_in[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
di_in[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
di_in[2]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
di_in[1]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
di_in[0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
vp_in2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
XADC2default:default2
vn_in2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
1502default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH1B2default:default2#
readAddress[11]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2532default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH1B2default:default2#
readAddress[10]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2532default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH1B2default:default2"
readAddress[9]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2532default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH1B2default:default2"
readAddress[8]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2532default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH1B2default:default2"
readAddress[7]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2532default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH1B2default:default2"
readAddress[6]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2532default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH1B2default:default2"
readAddress[5]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2532default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH1B2default:default2"
readAddress[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2532default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH1B2default:default2"
readAddress[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2532default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH1B2default:default2"
readAddress[2]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2532default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH1B2default:default2"
readAddress[1]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2532default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH1B2default:default2"
readAddress[0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2532default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH2B2default:default2#
readAddress[11]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2812default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH2B2default:default2#
readAddress[10]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2812default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH2B2default:default2"
readAddress[9]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2812default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH2B2default:default2"
readAddress[8]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2812default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH2B2default:default2"
readAddress[7]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2812default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH2B2default:default2"
readAddress[6]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2812default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH2B2default:default2"
readAddress[5]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2812default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH2B2default:default2"
readAddress[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2812default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH2B2default:default2"
readAddress[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2812default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH2B2default:default2"
readAddress[2]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2812default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH2B2default:default2"
readAddress[1]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2812default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
	BUFF_CH2B2default:default2"
readAddress[0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
2812default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter4[6]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter4[5]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter4[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter4[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter4[2]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter4[1]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter4[0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter3[6]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter3[5]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter3[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter3[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter3[2]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter3[1]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter3[0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter2[6]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter2[5]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter2[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter2[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter2[2]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter2[1]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
TEXT2default:default2$
maxCharacter2[0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
6652default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character24[6]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character24[5]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character24[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character24[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character24[2]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character24[1]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character24[0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character23[6]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character23[5]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character23[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character23[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character23[2]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character23[1]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character23[0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character22[6]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character22[5]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character22[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character22[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character22[2]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character22[1]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2"
character22[0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2!
character3[6]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2!
character3[5]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2!
character3[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2!
character3[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2!
character3[2]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2!
character3[1]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2!
character3[0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2'
character3Color[11]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2'
character3Color[10]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2&
character3Color[9]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2&
character3Color[8]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2&
character3Color[7]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2&
character3Color[6]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2&
character3Color[5]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2&
character3Color[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
LOOKUP2default:default2&
character3Color[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/OSEEAoscope_TOP.v2default:default2
7552default:default8@Z8-3295h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-32952default:default2
1002default:defaultZ17-14h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:04 ; elapsed = 00:00:04 . Memory (MB): peak = 537.758 ; gain = 191.785
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:00:04 ; elapsed = 00:00:04 . Memory (MB): peak = 537.758 ; gain = 191.785
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
e
-Analyzing %s Unisim elements for replacement
17*netlist2
12default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
W
Loading part %s157*device2$
xc7a100tcsg324-12default:defaultZ21-403h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
>

Processing XDC Constraints
244*projectZ1-262h px� 
=
Initializing timing engine
348*projectZ1-569h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2|
fc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/clk_wiz_0/clk_wiz_0/clk_wiz_0_in_context.xdc2default:default2!
clk_wiz_vga	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2|
fc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/clk_wiz_0/clk_wiz_0/clk_wiz_0_in_context.xdc2default:default2!
clk_wiz_vga	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2$
BUFF_CH1/bram0	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2$
BUFF_CH1/bram0	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2$
BUFF_CH1/bram1	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2$
BUFF_CH1/bram1	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2%
BUFF_CH1B/bram0	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2%
BUFF_CH1B/bram0	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2%
BUFF_CH1B/bram1	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2%
BUFF_CH1B/bram1	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2$
BUFF_CH2/bram0	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2$
BUFF_CH2/bram0	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2$
BUFF_CH2/bram1	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2$
BUFF_CH2/bram1	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2%
BUFF_CH2B/bram0	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2%
BUFF_CH2B/bram0	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2%
BUFF_CH2B/bram1	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
rc:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/ip/blk_mem_gen_0/blk_mem_gen_0/blk_mem_gen_0_in_context.xdc2default:default2%
BUFF_CH2B/bram1	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
�C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.runs/synth_1/.Xil/Vivado-13668-DESKTOP-PNS2FA1/CharactersROM/CharactersROM/CharactersROM_in_context.xdc2default:default2(
TEXT/characterBRAM	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
�C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.runs/synth_1/.Xil/Vivado-13668-DESKTOP-PNS2FA1/CharactersROM/CharactersROM/CharactersROM_in_context.xdc2default:default2(
TEXT/characterBRAM	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
�C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.runs/synth_1/.Xil/Vivado-13668-DESKTOP-PNS2FA1/CharactersROM/CharactersROM/CharactersROM_in_context.xdc2default:default2+
LOOKUP/characterBRAM2	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
�C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.runs/synth_1/.Xil/Vivado-13668-DESKTOP-PNS2FA1/CharactersROM/CharactersROM/CharactersROM_in_context.xdc2default:default2+
LOOKUP/characterBRAM2	2default:default8Z20-847h px� 
�
Parsing XDC File [%s]
179*designutils2q
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/constrs_1/new/OSEEAoscope_constr.xdc2default:default8Z20-179h px� 
�
Finished Parsing XDC File [%s]
178*designutils2q
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/constrs_1/new/OSEEAoscope_constr.xdc2default:default8Z20-178h px� 
�
�Implementation specific constraints were found while reading constraint file [%s]. These constraints will be ignored for synthesis but will be used in implementation. Impacted constraints are listed in the file [%s].
233*project2o
[C:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/constrs_1/new/OSEEAoscope_constr.xdc2default:default25
!.Xil/OSEEAoscope_TOP_propImpl.xdc2default:defaultZ1-236h px� 
H
&Completed Processing XDC Constraints

245*projectZ1-263h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common24
 Constraint Validation Runtime : 2default:default2
00:00:002default:default2 
00:00:00.0112default:default2
903.0272default:default2
0.0002default:defaultZ17-268h px� 
�
�Clock period '%s' specified during out-of-context synthesis of instance '%s' at clock pin '%s' is different from the actual clock period '%s', this can lead to different synthesis results.
203*timing2
20.0002default:default2"
BUFF_CH1/bram02default:default2
clka2default:default2
9.2592default:defaultZ38-316h px� 
�
�Clock period '%s' specified during out-of-context synthesis of instance '%s' at clock pin '%s' is different from the actual clock period '%s', this can lead to different synthesis results.
203*timing2
20.0002default:default2"
BUFF_CH1/bram12default:default2
clka2default:default2
9.2592default:defaultZ38-316h px� 
�
�Clock period '%s' specified during out-of-context synthesis of instance '%s' at clock pin '%s' is different from the actual clock period '%s', this can lead to different synthesis results.
203*timing2
20.0002default:default2#
BUFF_CH1B/bram02default:default2
clka2default:default2
9.2592default:defaultZ38-316h px� 
�
�Clock period '%s' specified during out-of-context synthesis of instance '%s' at clock pin '%s' is different from the actual clock period '%s', this can lead to different synthesis results.
203*timing2
20.0002default:default2#
BUFF_CH1B/bram12default:default2
clka2default:default2
9.2592default:defaultZ38-316h px� 
�
�Clock period '%s' specified during out-of-context synthesis of instance '%s' at clock pin '%s' is different from the actual clock period '%s', this can lead to different synthesis results.
203*timing2
20.0002default:default2"
BUFF_CH2/bram02default:default2
clka2default:default2
9.2592default:defaultZ38-316h px� 
�
�Clock period '%s' specified during out-of-context synthesis of instance '%s' at clock pin '%s' is different from the actual clock period '%s', this can lead to different synthesis results.
203*timing2
20.0002default:default2"
BUFF_CH2/bram12default:default2
clka2default:default2
9.2592default:defaultZ38-316h px� 
�
�Clock period '%s' specified during out-of-context synthesis of instance '%s' at clock pin '%s' is different from the actual clock period '%s', this can lead to different synthesis results.
203*timing2
20.0002default:default2#
BUFF_CH2B/bram02default:default2
clka2default:default2
9.2592default:defaultZ38-316h px� 
�
�Clock period '%s' specified during out-of-context synthesis of instance '%s' at clock pin '%s' is different from the actual clock period '%s', this can lead to different synthesis results.
203*timing2
20.0002default:default2#
BUFF_CH2B/bram12default:default2
clka2default:default2
9.2592default:defaultZ38-316h px� 
�
�Clock period '%s' specified during out-of-context synthesis of instance '%s' at clock pin '%s' is different from the actual clock period '%s', this can lead to different synthesis results.
203*timing2
20.0002default:default2)
LOOKUP/characterBRAM22default:default2
clka2default:default2
9.2592default:defaultZ38-316h px� 
�
�Clock period '%s' specified during out-of-context synthesis of instance '%s' at clock pin '%s' is different from the actual clock period '%s', this can lead to different synthesis results.
203*timing2
20.0002default:default2&
TEXT/characterBRAM2default:default2
clka2default:default2
9.2592default:defaultZ38-316h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
~Finished Constraint Validation : Time (s): cpu = 00:00:13 ; elapsed = 00:00:14 . Memory (MB): peak = 903.027 ; gain = 557.055
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Loading part: xc7a100tcsg324-1
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:13 ; elapsed = 00:00:14 . Memory (MB): peak = 903.027 ; gain = 557.055
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Z
%s
*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:00:13 ; elapsed = 00:00:14 . Memory (MB): peak = 903.027 ; gain = 557.055
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
u
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
count2default:defaultZ8-5546h px� 
u
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
clean2default:defaultZ8-5546h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2e
OC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/settings.v2default:default2
922default:default8@Z8-5818h px� 
u
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
count2default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys21
verticalScaleExponentChannel12default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys21
verticalScaleExponentChannel22default:defaultZ8-5546h px� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2
	state_reg2default:default2"
toggle_channel2default:defaultZ8-802h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
	DRPEnable2default:default2
22default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
	DRPEnable2default:default2
22default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
merging register '%s' into '%s'3619*oasys2&
ram1_din_reg[11:0]2default:default2&
ram0_din_reg[11:0]2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
512default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2 
ram1_din_reg2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
512default:default8@Z8-6014h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
362default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
562default:default8@Z8-5818h px� 
�
merging register '%s' into '%s'3619*oasys2&
ram1_din_reg[11:0]2default:default2&
ram0_din_reg[11:0]2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
512default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2 
ram1_din_reg2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
512default:default8@Z8-6014h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
362default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
562default:default8@Z8-5818h px� 
�
merging register '%s' into '%s'3619*oasys2&
ram1_din_reg[11:0]2default:default2&
ram0_din_reg[11:0]2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
512default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2 
ram1_din_reg2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
512default:default8@Z8-6014h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
362default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
562default:default8@Z8-5818h px� 
�
merging register '%s' into '%s'3619*oasys2&
ram1_din_reg[11:0]2default:default2&
ram0_din_reg[11:0]2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
512default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2 
ram1_din_reg2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
512default:default8@Z8-6014h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
362default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2c
MC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/buffer.v2default:default2
562default:default8@Z8-5818h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2+
samplePeriodPlusOne_reg2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/get_timeDiv.v2default:default2
302default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2+
samplePeriodPlusOne_reg2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/get_timeDiv.v2default:default2
302default:default8@Z8-6014h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3532default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3492default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3452default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3412default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3372default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3332default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3292default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3252default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3212default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3172default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3132default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3092default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3052default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3012default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2972default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2922default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2882default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2842default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2802default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2762default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2722default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2682default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2642default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2602default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2562default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2512default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2472default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2432default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2392default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2352default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2302default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2262default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2222default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2182default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2142default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2092default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2052default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2012default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
1972default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
1932default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3532default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3492default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3452default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3412default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3372default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3332default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3292default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3252default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3212default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3172default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3132default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3092default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3052default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
3012default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2972default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2922default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2882default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2842default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2802default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2762default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2722default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2682default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2642default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2602default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2562default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2512default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2472default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2432default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2392default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2352default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2302default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2262default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2222default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2182default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2142default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2092default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2052default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
2012default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
1972default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2a
KC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/text.v2default:default2
1932default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
3092default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
3052default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
3012default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
2972default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
2922default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
2882default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
2842default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
2802default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
2762default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
2712default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2h
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/lookup_text.v2default:default2
2672default:default8@Z8-5818h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-58182default:default2
1002default:defaultZ17-14h px� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2t
`                   State |                     New Encoding |                Previous Encoding 
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2s
_                    IDLE |                               00 |                               00
2default:defaulth p
x
� 
�
%s
*synth2s
_          READ_CHANNEL_1 |                               01 |                               01
2default:defaulth p
x
� 
�
%s
*synth2s
_          READ_CHANNEL_2 |                               10 |                               10
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
Gencoded FSM with state register '%s' using encoding '%s' in module '%s'3353*oasys2
	state_reg2default:default2

sequential2default:default2"
toggle_channel2default:defaultZ8-3354h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:15 ; elapsed = 00:00:16 . Memory (MB): peak = 903.027 ; gain = 557.055
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
d
%s
*synth2L
8+------+----------------------+------------+----------+
2default:defaulth p
x
� 
d
%s
*synth2L
8|      |RTL Partition         |Replication |Instances |
2default:defaulth p
x
� 
d
%s
*synth2L
8+------+----------------------+------------+----------+
2default:defaulth p
x
� 
d
%s
*synth2L
8|1     |Text__GB0             |           1|     29577|
2default:defaulth p
x
� 
d
%s
*synth2L
8|2     |Text__GB1             |           1|      6719|
2default:defaulth p
x
� 
d
%s
*synth2L
8|3     |Text__GB2             |           1|      8099|
2default:defaulth p
x
� 
d
%s
*synth2L
8|4     |lookup_text__GB0      |           1|     27268|
2default:defaulth p
x
� 
d
%s
*synth2L
8|5     |lookup_text__GB1      |           1|      7226|
2default:defaulth p
x
� 
d
%s
*synth2L
8|6     |OSEEAoscope_TOP__GCB0 |           1|     18391|
2default:defaulth p
x
� 
d
%s
*synth2L
8|7     |OSEEAoscope_TOP__GCB1 |           1|      7286|
2default:defaulth p
x
� 
d
%s
*synth2L
8+------+----------------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 19    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input     32 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     21 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     20 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     13 Bit       Adders := 145   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 21    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     12 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit       Adders := 36    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      5 Bit       Adders := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 28    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              100 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               21 Bit    Registers := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               20 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               17 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               14 Bit    Registers := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 126   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                7 Bit    Registers := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                6 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 17    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 18    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 99    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 14    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     22 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     20 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 21    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  24 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit        Muxes := 13    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      6 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  12 Input      4 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      2 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  41 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  30 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 59    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   8 Input      1 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      1 Bit        Muxes := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  12 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Hierarchical RTL Component report 
2default:defaulth p
x
� 
D
%s
*synth2,
Module OSEEAoscope_TOP 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     22 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
Module Text 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     13 Bit       Adders := 82    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      5 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               17 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 15    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 15    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  41 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
@
%s
*synth2(
Module lookup_text 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     13 Bit       Adders := 60    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      5 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               17 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 15    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 15    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  30 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
\
%s
*synth2D
0Module draw_horizontal_line__parameterized0__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
I
%s
*synth21
Module cursor_to_voltage__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               21 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
F
%s
*synth2.
Module binary2decimal__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit       Adders := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 7     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                7 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
I
%s
*synth21
Module cursor_to_voltage__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               21 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
F
%s
*synth2.
Module binary2decimal__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit       Adders := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 7     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                7 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
Y
%s
*synth2A
-Module draw_horizontal_line__parameterized0 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
I
%s
*synth21
Module draw_horizontal_line 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     13 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module draw_wave__parameterized0 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     21 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     13 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
Module settings 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     20 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               20 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                6 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     20 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit        Muxes := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      6 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 11    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   8 Input      1 Bit        Muxes := 7     
2default:defaulth p
x
� 
>
%s
*synth2&
Module draw_wave 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     21 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     13 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
Module Grid 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              100 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  24 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
B
%s
*synth2*
Module xvga1280_1024 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
F
%s
*synth2.
Module measure_signal__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 5     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
F
%s
*synth2.
Module buffer__xdcDup__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 3     
2default:defaulth p
x
� 
F
%s
*synth2.
Module buffer__xdcDup__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 3     
2default:defaulth p
x
� 
B
%s
*synth2*
Module buffer_select 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
G
%s
*synth2/
Module low_pass_filter__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     12 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               14 Bit    Registers := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
F
%s
*synth2.
Module buffer__xdcDup__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 3     
2default:defaulth p
x
� 
D
%s
*synth2,
Module low_pass_filter 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     12 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               14 Bit    Registers := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
;
%s
*synth2#
Module buffer 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 3     
2default:defaulth p
x
� 
C
%s
*synth2+
Module channel_toggle 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
?
%s
*synth2'
Module sw_trigger 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
C
%s
*synth2+
Module measure_signal 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 5     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
C
%s
*synth2+
Module adc_controller 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
C
%s
*synth2+
Module toggle_channel 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                7 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      2 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      1 Bit        Muxes := 6     
2default:defaulth p
x
� 
I
%s
*synth21
Module vertScale_conversion 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  12 Input      4 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  12 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
@
%s
*synth2(
Module one_shot__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
@
%s
*synth2(
Module one_shot__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
@
%s
*synth2(
Module one_shot__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
Module one_shot 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
@
%s
*synth2(
Module debounce__1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 6     
2default:defaulth p
x
� 
@
%s
*synth2(
Module debounce__2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 6     
2default:defaulth p
x
� 
C
%s
*synth2+
Module binary2decimal 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit       Adders := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 7     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                7 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
F
%s
*synth2.
Module cursor_to_voltage 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               21 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               12 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
F
%s
*synth2.
Module binary2decimal__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit       Adders := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 7     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                7 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
@
%s
*synth2(
Module debounce__3 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 6     
2default:defaulth p
x
� 
=
%s
*synth2%
Module debounce 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 6     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2k
WPart Resources:
DSPs: 240 (col length:80)
BRAMs: 270 (col length: RAMB18 80 RAMB36 40)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
]
%s
*synth2E
1Warning: Parallel synthesis criteria is not met 
2default:defaulth p
x
� 
�
RFound unconnected internal register '%s' and it is trimmed from '%s' to '%s' bits.3455*oasys28
$CURSOR2VOLT/voltageAbsoluteValue_reg2default:default2
122default:default2
102default:default2n
XC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/cursor_to_voltage.v2default:default2
402default:default8@Z8-3936h px� 
�
RFound unconnected internal register '%s' and it is trimmed from '%s' to '%s' bits.3455*oasys28
$CURSOR2VOLT/voltageAbsoluteValue_reg2default:default2
122default:default2
102default:default2n
XC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/cursor_to_voltage.v2default:default2
402default:default8@Z8-3936h px� 
h
%s
*synth2P
<DSP Report: Generating DSP bigMult, operation Mode is: A*B.
2default:defaulth p
x
� 
g
%s
*synth2O
;DSP Report: operator bigMult is absorbed into DSP bigMult.
2default:defaulth p
x
� 
h
%s
*synth2P
<DSP Report: Generating DSP bigMult, operation Mode is: A*B.
2default:defaulth p
x
� 
g
%s
*synth2O
;DSP Report: operator bigMult is absorbed into DSP bigMult.
2default:defaulth p
x
� 
�
merging register '%s' into '%s'3619*oasys2+
signalAverage_reg[11:0]2default:default2+
signalAverage_reg[11:0]2default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/measure_signal.v2default:default2
462default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2%
signalAverage_reg2default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/measure_signal.v2default:default2
462default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys20
previousSamples_reg[1][11:0]2default:default20
previousSamples_reg[1][11:0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-4471h px� 
�
merging register '%s' into '%s'3619*oasys20
previousSamples_reg[2][11:0]2default:default20
previousSamples_reg[2][11:0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2*
previousSamples_reg[1]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2*
previousSamples_reg[2]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys20
previousSamples_reg[3][11:0]2default:default20
previousSamples_reg[3][11:0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-4471h px� 
�
merging register '%s' into '%s'3619*oasys20
previousSamples_reg[4][11:0]2default:default20
previousSamples_reg[4][11:0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2*
previousSamples_reg[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2*
previousSamples_reg[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-6014h px� 
�
%s
*synth2h
TDSP Report: Generating DSP intermediates_reg[4], operation Mode is: (A''*(B:0x2))'.
2default:defaulth p
x
� 
�
%s
*synth2k
WDSP Report: register previousSamples_reg[1] is absorbed into DSP intermediates_reg[4].
2default:defaulth p
x
� 
�
%s
*synth2k
WDSP Report: register previousSamples_reg[2] is absorbed into DSP intermediates_reg[4].
2default:defaulth p
x
� 
�
%s
*synth2i
UDSP Report: register intermediates_reg[4] is absorbed into DSP intermediates_reg[4].
2default:defaulth p
x
� 
t
%s
*synth2\
HDSP Report: operator p_0_out is absorbed into DSP intermediates_reg[4].
2default:defaulth p
x
� 
�
%s
*synth2j
VDSP Report: Generating DSP intermediates_reg[3], operation Mode is: (ACIN2*(B:0x3))'.
2default:defaulth p
x
� 
�
%s
*synth2k
WDSP Report: register previousSamples_reg[3] is absorbed into DSP intermediates_reg[3].
2default:defaulth p
x
� 
�
%s
*synth2i
UDSP Report: register intermediates_reg[3] is absorbed into DSP intermediates_reg[3].
2default:defaulth p
x
� 
t
%s
*synth2\
HDSP Report: operator p_0_out is absorbed into DSP intermediates_reg[3].
2default:defaulth p
x
� 
�
%s
*synth2j
VDSP Report: Generating DSP intermediates_reg[2], operation Mode is: (ACIN2*(B:0x2))'.
2default:defaulth p
x
� 
�
%s
*synth2k
WDSP Report: register previousSamples_reg[4] is absorbed into DSP intermediates_reg[2].
2default:defaulth p
x
� 
�
%s
*synth2i
UDSP Report: register intermediates_reg[2] is absorbed into DSP intermediates_reg[2].
2default:defaulth p
x
� 
t
%s
*synth2\
HDSP Report: operator p_0_out is absorbed into DSP intermediates_reg[2].
2default:defaulth p
x
� 
�
merging register '%s' into '%s'3619*oasys20
previousSamples_reg[1][11:0]2default:default20
previousSamples_reg[1][11:0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-4471h px� 
�
merging register '%s' into '%s'3619*oasys20
previousSamples_reg[2][11:0]2default:default20
previousSamples_reg[2][11:0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2*
previousSamples_reg[1]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2*
previousSamples_reg[2]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys20
previousSamples_reg[3][11:0]2default:default20
previousSamples_reg[3][11:0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-4471h px� 
�
merging register '%s' into '%s'3619*oasys20
previousSamples_reg[4][11:0]2default:default20
previousSamples_reg[4][11:0]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2*
previousSamples_reg[3]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2*
previousSamples_reg[4]2default:default2l
VC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/low_pass_filter.v2default:default2
372default:default8@Z8-6014h px� 
�
%s
*synth2h
TDSP Report: Generating DSP intermediates_reg[4], operation Mode is: (A''*(B:0x2))'.
2default:defaulth p
x
� 
�
%s
*synth2k
WDSP Report: register previousSamples_reg[1] is absorbed into DSP intermediates_reg[4].
2default:defaulth p
x
� 
�
%s
*synth2k
WDSP Report: register previousSamples_reg[2] is absorbed into DSP intermediates_reg[4].
2default:defaulth p
x
� 
�
%s
*synth2i
UDSP Report: register intermediates_reg[4] is absorbed into DSP intermediates_reg[4].
2default:defaulth p
x
� 
t
%s
*synth2\
HDSP Report: operator p_0_out is absorbed into DSP intermediates_reg[4].
2default:defaulth p
x
� 
�
%s
*synth2j
VDSP Report: Generating DSP intermediates_reg[3], operation Mode is: (ACIN2*(B:0x3))'.
2default:defaulth p
x
� 
�
%s
*synth2k
WDSP Report: register previousSamples_reg[3] is absorbed into DSP intermediates_reg[3].
2default:defaulth p
x
� 
�
%s
*synth2i
UDSP Report: register intermediates_reg[3] is absorbed into DSP intermediates_reg[3].
2default:defaulth p
x
� 
t
%s
*synth2\
HDSP Report: operator p_0_out is absorbed into DSP intermediates_reg[3].
2default:defaulth p
x
� 
�
%s
*synth2j
VDSP Report: Generating DSP intermediates_reg[2], operation Mode is: (ACIN2*(B:0x2))'.
2default:defaulth p
x
� 
�
%s
*synth2k
WDSP Report: register previousSamples_reg[4] is absorbed into DSP intermediates_reg[2].
2default:defaulth p
x
� 
�
%s
*synth2i
UDSP Report: register intermediates_reg[2] is absorbed into DSP intermediates_reg[2].
2default:defaulth p
x
� 
t
%s
*synth2\
HDSP Report: operator p_0_out is absorbed into DSP intermediates_reg[2].
2default:defaulth p
x
� 
�
merging register '%s' into '%s'3619*oasys2+
signalAverage_reg[11:0]2default:default2+
signalAverage_reg[11:0]2default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/measure_signal.v2default:default2
462default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2%
signalAverage_reg2default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/measure_signal.v2default:default2
462default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2'
dataOutChannel1_reg2default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/adc_controller.v2default:default2
382default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2'
dataOutChannel2_reg2default:default2k
UC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/adc_controller.v2default:default2
392default:default8@Z8-6014h px� 
�
%s
*synth2q
]DSP Report: Generating DSP dataInChannel1NumeratorScaled, operation Mode is: (A*(B:0x3e8))'.
2default:defaulth p
x
� 
w
%s
*synth2_
KDSP Report: register P is absorbed into DSP dataInChannel1NumeratorScaled.
2default:defaulth p
x
� 
�
%s
*synth2{
gDSP Report: operator dataInChannel1NumeratorScaled is absorbed into DSP dataInChannel1NumeratorScaled.
2default:defaulth p
x
� 
�
%s
*synth2q
]DSP Report: Generating DSP dataInChannel2NumeratorScaled, operation Mode is: (A*(B:0x3e8))'.
2default:defaulth p
x
� 
w
%s
*synth2_
KDSP Report: register P is absorbed into DSP dataInChannel2NumeratorScaled.
2default:defaulth p
x
� 
�
%s
*synth2{
gDSP Report: operator dataInChannel2NumeratorScaled is absorbed into DSP dataInChannel2NumeratorScaled.
2default:defaulth p
x
� 
�
RFound unconnected internal register '%s' and it is trimmed from '%s' to '%s' bits.3455*oasys28
$CURSOR2VOLT/voltageAbsoluteValue_reg2default:default2
122default:default2
102default:default2n
XC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.srcs/sources_1/new/cursor_to_voltage.v2default:default2
402default:default8@Z8-3936h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2<
(VERT_SCALE/verticalScaleExponentChannel12default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2<
(VERT_SCALE/verticalScaleExponentChannel22default:defaultZ8-5546h px� 
z
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2

AUTO/count2default:defaultZ8-5546h px� 
z
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2

AUTO/clean2default:defaultZ8-5546h px� 
z
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2

DOWN/count2default:defaultZ8-5546h px� 
z
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2

DOWN/clean2default:defaultZ8-5546h px� 
x
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
UP/count2default:defaultZ8-5546h px� 
x
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
UP/clean2default:defaultZ8-5546h px� 
|
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2 
CENTER/count2default:defaultZ8-5546h px� 
|
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2 
CENTER/clean2default:defaultZ8-5546h px� 
g
%s
*synth2O
;DSP Report: Generating DSP level4, operation Mode is: A*B.
2default:defaulth p
x
� 
e
%s
*synth2M
9DSP Report: operator level4 is absorbed into DSP level4.
2default:defaulth p
x
� 
v
%s
*synth2^
JDSP Report: Generating DSP level1, operation Mode is: ((C:0x7) or 0)+A*B.
2default:defaulth p
x
� 
e
%s
*synth2M
9DSP Report: operator level2 is absorbed into DSP level1.
2default:defaulth p
x
� 
e
%s
*synth2M
9DSP Report: operator level4 is absorbed into DSP level1.
2default:defaulth p
x
� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2,
i_0/BUFF_CH1/ram1_we_reg2default:default2
FDE2default:default2,
i_0/BUFF_CH2/ram1_we_reg2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR2_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[7]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR1_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[6]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR1_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[5]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR1_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[4]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR1_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[3]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR1_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[2]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR1_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[1]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR1_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[0]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;i_0/CURSOR2_TOTEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[2]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;i_0/CURSOR2_TOTEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[1]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;i_0/CURSOR2_TOTEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[0]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR2_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[0]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR2_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[1]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR2_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[2]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR2_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[3]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR2_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[4]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR2_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[5]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR2_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[6]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;i_0/CURSOR1_TOTEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[0]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;i_0/CURSOR1_TOTEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[1]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;i_0/CURSOR1_TOTEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[2]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2I
5i_0/CURSOR1_TOTEXT/CURSOR2VOLT/pixelsToVoltage_reg[7]2default:default2
FD2default:default21
i_0/GRID/gridDisplayY_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[99]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[98]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[97]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[96]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[95]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[94]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[93]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[92]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[91]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[90]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[89]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[88]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[87]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[86]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[85]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[84]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[83]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[82]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[81]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[80]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[79]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[78]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[77]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[76]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[75]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[74]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[73]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[72]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[71]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[70]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[69]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[68]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[67]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[66]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[65]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[64]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[63]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[62]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[61]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[60]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[59]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[58]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[57]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[56]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[55]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[54]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[53]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[52]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[51]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[50]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[49]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[48]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[47]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[46]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[45]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[44]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[43]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[42]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[41]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[40]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[39]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[38]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[37]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[36]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[35]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[34]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[33]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[32]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[31]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[30]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[29]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[28]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[27]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[26]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[25]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[24]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
i_0/GRID/pixel_reg[23]2default:default2
FDR2default:default2)
i_0/GRID/pixel_reg[0]2default:defaultZ8-3886h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-38862default:default2
1002default:defaultZ17-14h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2+
i_0/\GRID/pixel_reg[9] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default23
i_0/\GRID/gridDisplayX_reg[11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2B
.i_0/\CURSOR2_TOTEXT/BIN2DEC/character2_reg[4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2=
)i_0/\DRAW_WAVE_CH1/curveDisplayY_reg[11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2=
)i_0/\DRAW_WAVE_CH1/curveDisplayY_reg[11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2=
)i_0/\DRAW_WAVE_CH1/curveDisplayY_reg[11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2=
)i_0/\DRAW_WAVE_CH1/curveDisplayY_reg[11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2=
)i_0/\DRAW_WAVE_CH1/curveDisplayY_reg[11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2N
:i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltage_reg[14] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2N
:i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltage_reg[10] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2N
:i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltage_reg[12] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2M
9i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltage_reg[8] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2N
:i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltage_reg[13] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2M
9i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltage_reg[9] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2N
:i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltage_reg[11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2N
:i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltage_reg[15] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2M
9i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltage_reg[7] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2N
:i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltage_reg[17] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2N
:i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltage_reg[16] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default29
%i_1/\TIME_DIV/timePerDivision_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default26
"i_1/\TOGGLE_CHA/DRPAddress_reg[6] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default27
#i_1/\TOGGLE_CHA/DRPWriteEnable_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default26
"i_1/\FILTER/estimatedSlope_reg[4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2D
0i_1/\VOLTSDIV_TO_TEXT/BIN2DEC/character0_reg[4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2D
0i_1/\VOLTSDIV_TO_TEXT/BIN2DEC/character1_reg[4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2D
0i_1/\VOLTSDIV_TO_TEXT/BIN2DEC/character2_reg[4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2;
'i_1/\BIN2DEC_TIMEDIV/character2_reg[4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2;
'i_1/\BIN2DEC_TIMEDIV/character1_reg[4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2;
'i_1/\BIN2DEC_TIMEDIV/character0_reg[4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2T
@i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[17] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2T
@i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[13] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[15] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2T
@i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[16] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[12] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[14] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[18] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[10] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[20] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2T
@i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaled_reg[19] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2=
)i_1/\BIN2DEC_TIMEDIV/previousData_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2@
,i_1/\BIN2DEC_TIMEDIV/remainingData10_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2\
Hi_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[7] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2\
Hi_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2\
Hi_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[5] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2\
Hi_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[1] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2\
Hi_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[6] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2\
Hi_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2\
Hi_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2\
Hi_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[8] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2\
Hi_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2]
Ii_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[17] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2\
Hi_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[9] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default22
i_1/\BIN2DEC_TIMEDIV/d_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2F
2i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/voltage_reg[11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2E
1i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/voltage_reg[8] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2E
1i_1/\VOLTSDIV_TO_TEXT/CURSOR2VOLT/voltage_reg[9] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2;
'i_1/\BIN2DEC_TIMEDIV/character0_reg[0] 2default:defaultZ8-3333h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:00:40 ; elapsed = 00:00:42 . Memory (MB): peak = 1238.375 ; gain = 892.402
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
_
%s*synth2G
3
DSP: Preliminary Mapping  Report (see note below)
2default:defaulth px� 
�
%s*synth2�
�+----------------+--------------------+--------+--------+--------+--------+--------+------+------+------+------+-------+------+------+
2default:defaulth px� 
�
%s*synth2�
�|Module Name     | DSP Mapping        | A Size | B Size | C Size | D Size | P Size | AREG | BREG | CREG | DREG | ADREG | MREG | PREG | 
2default:defaulth px� 
�
%s*synth2�
�+----------------+--------------------+--------+--------+--------+--------+--------+------+------+------+------+-------+------+------+
2default:defaulth px� 
�
%s*synth2�
�|draw_wave       | A*B                | 21     | 10     | -      | -      | 31     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|draw_wave       | A*B                | 21     | 10     | -      | -      | 31     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|low_pass_filter | (A''*(B:0x2))'     | 12     | 3      | -      | -      | 15     | 2    | 0    | -    | -    | -     | 1    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|low_pass_filter | (ACIN2*(B:0x3))'   | 12     | 3      | -      | -      | 15     | 1    | 0    | -    | -    | -     | 1    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|low_pass_filter | (ACIN2*(B:0x2))'   | 12     | 3      | -      | -      | 15     | 1    | 0    | -    | -    | -     | 1    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|low_pass_filter | (A''*(B:0x2))'     | 12     | 3      | -      | -      | 15     | 2    | 0    | -    | -    | -     | 1    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|low_pass_filter | (ACIN2*(B:0x3))'   | 12     | 3      | -      | -      | 15     | 1    | 0    | -    | -    | -     | 1    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|low_pass_filter | (ACIN2*(B:0x2))'   | 12     | 3      | -      | -      | 15     | 1    | 0    | -    | -    | -     | 1    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|adc_controller  | (A*(B:0x3e8))'     | 12     | 11     | -      | -      | 22     | 0    | 0    | -    | -    | -     | 1    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|adc_controller  | (A*(B:0x3e8))'     | 12     | 11     | -      | -      | 22     | 0    | 0    | -    | -    | -     | 1    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|OSEEAoscope_TOP | A*B                | 12     | 10     | -      | -      | 22     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�|OSEEAoscope_TOP | ((C:0x7) or 0)+A*B | 12     | 10     | 4      | -      | 15     | 0    | 0    | 0    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�+----------------+--------------------+--------+--------+--------+--------+--------+------+------+------+------+-------+------+------+

2default:defaulth px� 
�
%s*synth2�
�Note: The table above is a preliminary report that shows the DSPs inferred at the current stage of the synthesis flow. Some DSP may be reimplemented as non DSP primitives later in the synthesis flow. Multiple instantiated DSPs are reported only once.
2default:defaulth px� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
d
%s
*synth2L
8+------+----------------------+------------+----------+
2default:defaulth p
x
� 
d
%s
*synth2L
8|      |RTL Partition         |Replication |Instances |
2default:defaulth p
x
� 
d
%s
*synth2L
8+------+----------------------+------------+----------+
2default:defaulth p
x
� 
d
%s
*synth2L
8|1     |Text__GB0             |           1|     24847|
2default:defaulth p
x
� 
d
%s
*synth2L
8|2     |Text__GB1             |           1|      4353|
2default:defaulth p
x
� 
d
%s
*synth2L
8|3     |Text__GB2             |           1|      5219|
2default:defaulth p
x
� 
d
%s
*synth2L
8|4     |lookup_text__GB0      |           1|      1640|
2default:defaulth p
x
� 
d
%s
*synth2L
8|5     |lookup_text__GB1      |           1|       379|
2default:defaulth p
x
� 
d
%s
*synth2L
8|6     |OSEEAoscope_TOP__GCB0 |           1|      8039|
2default:defaulth p
x
� 
d
%s
*synth2L
8|7     |OSEEAoscope_TOP__GCB1 |           1|      3313|
2default:defaulth p
x
� 
d
%s
*synth2L
8+------+----------------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
R
%s
*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
2Moved timing constraint from pin '%s' to pin '%s'
4028*oasys2(
clk_wiz_vga/clk_out12default:default21
clk_wiz_vga/bbstub_clk_out1/O2default:defaultZ8-5578h px� 
�
SMoved %s constraints on hierarchical pins to their respective driving/loading pins
4235*oasys2
12default:defaultZ8-5819h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:00:51 ; elapsed = 00:00:54 . Memory (MB): peak = 1238.375 ; gain = 892.402
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
}Finished Timing Optimization : Time (s): cpu = 00:01:06 ; elapsed = 00:01:09 . Memory (MB): peak = 1238.375 ; gain = 892.402
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
b
%s
*synth2J
6+------+--------------------+------------+----------+
2default:defaulth p
x
� 
b
%s
*synth2J
6|      |RTL Partition       |Replication |Instances |
2default:defaulth p
x
� 
b
%s
*synth2J
6+------+--------------------+------------+----------+
2default:defaulth p
x
� 
b
%s
*synth2J
6|1     |Text__GB0           |           1|     24847|
2default:defaulth p
x
� 
b
%s
*synth2J
6|2     |Text__GB1           |           1|      4353|
2default:defaulth p
x
� 
b
%s
*synth2J
6|3     |Text__GB2           |           1|      5219|
2default:defaulth p
x
� 
b
%s
*synth2J
6|4     |lookup_text__GB0    |           1|      1640|
2default:defaulth p
x
� 
b
%s
*synth2J
6|5     |lookup_text__GB1    |           1|       379|
2default:defaulth p
x
� 
b
%s
*synth2J
6|6     |OSEEAoscope_TOP_GT0 |           1|     11137|
2default:defaulth p
x
� 
b
%s
*synth2J
6|7     |OSEEAoscope_TOP_GT1 |           1|        24|
2default:defaulth p
x
� 
b
%s
*synth2J
6+------+--------------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
|Finished Technology Mapping : Time (s): cpu = 00:01:13 ; elapsed = 00:01:16 . Memory (MB): peak = 1238.375 ; gain = 892.402
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
,Flop %s is being inverted and renamed to %s.3906*oasys29
%CH_TOGGLE/channelSelectedData_reg[11]2default:default2=
)CH_TOGGLE/channelSelectedData_reg[11]_inv2default:defaultZ8-5365h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2!
characterBRAM2default:default2
clkb2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2+
\LOOKUP/characterBRAM2 2default:default2
clkb2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram0 2default:default2
dinb[11]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram0 2default:default2
dinb[10]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram0 2default:default2
dinb[9]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram0 2default:default2
dinb[8]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram0 2default:default2
dinb[7]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram0 2default:default2
dinb[6]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram0 2default:default2
dinb[5]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram0 2default:default2
dinb[4]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram0 2default:default2
dinb[3]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram0 2default:default2
dinb[2]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram0 2default:default2
dinb[1]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram0 2default:default2
dinb[0]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram1 2default:default2
dinb[11]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram1 2default:default2
dinb[10]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram1 2default:default2
dinb[9]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram1 2default:default2
dinb[8]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram1 2default:default2
dinb[7]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram1 2default:default2
dinb[6]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram1 2default:default2
dinb[5]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram1 2default:default2
dinb[4]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram1 2default:default2
dinb[3]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram1 2default:default2
dinb[2]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram1 2default:default2
dinb[1]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH2/bram1 2default:default2
dinb[0]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram0 2default:default2
dinb[11]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram0 2default:default2
dinb[10]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram0 2default:default2
dinb[9]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram0 2default:default2
dinb[8]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram0 2default:default2
dinb[7]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram0 2default:default2
dinb[6]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram0 2default:default2
dinb[5]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram0 2default:default2
dinb[4]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram0 2default:default2
dinb[3]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram0 2default:default2
dinb[2]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram0 2default:default2
dinb[1]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram0 2default:default2
dinb[0]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram1 2default:default2
dinb[11]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram1 2default:default2
dinb[10]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram1 2default:default2
dinb[9]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram1 2default:default2
dinb[8]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram1 2default:default2
dinb[7]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram1 2default:default2
dinb[6]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram1 2default:default2
dinb[5]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram1 2default:default2
dinb[4]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram1 2default:default2
dinb[3]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram1 2default:default2
dinb[2]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram1 2default:default2
dinb[1]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2$
\BUFF_CH1/bram1 2default:default2
dinb[0]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram0 2default:default2
dinb[11]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram0 2default:default2
dinb[10]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram0 2default:default2
dinb[9]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram0 2default:default2
dinb[8]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram0 2default:default2
dinb[7]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram0 2default:default2
dinb[6]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram0 2default:default2
dinb[5]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram0 2default:default2
dinb[4]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram0 2default:default2
dinb[3]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram0 2default:default2
dinb[2]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram0 2default:default2
dinb[1]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram0 2default:default2
dinb[0]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram1 2default:default2
dinb[11]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram1 2default:default2
dinb[10]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram1 2default:default2
dinb[9]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram1 2default:default2
dinb[8]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram1 2default:default2
dinb[7]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram1 2default:default2
dinb[6]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram1 2default:default2
dinb[5]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram1 2default:default2
dinb[4]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram1 2default:default2
dinb[3]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram1 2default:default2
dinb[2]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram1 2default:default2
dinb[1]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH1B/bram1 2default:default2
dinb[0]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram0 2default:default2
dinb[11]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram0 2default:default2
dinb[10]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram0 2default:default2
dinb[9]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram0 2default:default2
dinb[8]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram0 2default:default2
dinb[7]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram0 2default:default2
dinb[6]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram0 2default:default2
dinb[5]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram0 2default:default2
dinb[4]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram0 2default:default2
dinb[3]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram0 2default:default2
dinb[2]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram0 2default:default2
dinb[1]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram0 2default:default2
dinb[0]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram1 2default:default2
dinb[11]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram1 2default:default2
dinb[10]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram1 2default:default2
dinb[9]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram1 2default:default2
dinb[8]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram1 2default:default2
dinb[7]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram1 2default:default2
dinb[6]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram1 2default:default2
dinb[5]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram1 2default:default2
dinb[4]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram1 2default:default2
dinb[3]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram1 2default:default2
dinb[2]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram1 2default:default2
dinb[1]2default:defaultZ8-4442h px� 
�
*BlackBox module %s has unconnected pin %s
3599*oasys2%
\BUFF_CH2B/bram1 2default:default2
dinb[0]2default:defaultZ8-4442h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
vFinished IO Insertion : Time (s): cpu = 00:01:15 ; elapsed = 00:01:17 . Memory (MB): peak = 1238.375 ; gain = 892.402
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:01:15 ; elapsed = 00:01:18 . Memory (MB): peak = 1238.375 ; gain = 892.402
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:01:16 ; elapsed = 00:01:18 . Memory (MB): peak = 1238.375 ; gain = 892.402
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:01:16 ; elapsed = 00:01:18 . Memory (MB): peak = 1238.375 ; gain = 892.402
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:01:16 ; elapsed = 00:01:19 . Memory (MB): peak = 1238.375 ; gain = 892.402
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:01:16 ; elapsed = 00:01:19 . Memory (MB): peak = 1238.375 ; gain = 892.402
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23

Static Shift Register Report:
2default:defaulth p
x
� 
�
%s
*synth2�
�+----------------+-------------------------------------------------------------------+--------+-------+--------------+--------------------+-------------------+--------+---------+
2default:defaulth p
x
� 
�
%s
*synth2�
�|Module Name     | RTL Name                                                          | Length | Width | Reset Signal | Pull out first Reg | Pull out last Reg | SRL16E | SRLC32E | 
2default:defaulth p
x
� 
�
%s
*synth2�
�+----------------+-------------------------------------------------------------------+--------+-------+--------------+--------------------+-------------------+--------+---------+
2default:defaulth p
x
� 
�
%s
*synth2�
�|Text            | displayY4_reg[11]                                                 | 3      | 12    | NO           | NO                 | YES               | 12     | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|Text            | blankOut_reg                                                      | 5      | 1     | NO           | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|Text            | vsyncOut_reg                                                      | 5      | 1     | NO           | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|Text            | previousPixel4_reg[11]                                            | 4      | 12    | NO           | NO                 | NO                | 12     | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|Text            | displayX4_reg[11]                                                 | 3      | 12    | NO           | NO                 | YES               | 12     | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|Text            | hsyncOut_reg                                                      | 5      | 1     | NO           | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | LOOKUP/displayY4_reg[11]                                          | 3      | 12    | NO           | NO                 | YES               | 12     | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | LOOKUP/blankOut_reg                                               | 5      | 1     | NO           | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | LOOKUP/previousPixel4_reg[11]                                     | 4      | 12    | NO           | NO                 | YES               | 12     | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | LOOKUP/displayX4_reg[11]                                          | 3      | 12    | NO           | NO                 | YES               | 12     | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | VGA_HS_reg                                                        | 6      | 1     | NO           | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | VGA_VS_reg                                                        | 6      | 1     | NO           | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | TRIGGER/genblk1[4].previousData_reg[5][11]                        | 6      | 12    | NO           | NO                 | YES               | 12     | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | FILTER/previousSmoothedSamples_reg[4][6]                          | 5      | 7     | NO           | NO                 | YES               | 7      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | FILTER2/previousSamples_reg[5][11]                                | 4      | 12    | NO           | NO                 | NO                | 12     | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | FILTER2/previousSmoothedSamples_reg[4][6]                         | 5      | 7     | NO           | NO                 | YES               | 7      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | FILTER/previousSamples_reg[5][11]                                 | 4      | 12    | NO           | NO                 | NO                | 12     | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | CURSOR2_TOTEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[11] | 3      | 10    | NO           | NO                 | YES               | 10     | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | CURSOR1_TOTEXT/CURSOR2VOLT/pixelsToVoltageScaledDivHeight_reg[11] | 3      | 10    | NO           | NO                 | YES               | 10     | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | CURSOR2/spriteDisplayX_reg[10]                                    | 4      | 11    | NO           | NO                 | YES               | 11     | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | CURSOR2/spriteHsync_reg                                           | 6      | 1     | NO           | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | CURSOR2/spriteVsync_reg                                           | 6      | 1     | NO           | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�|OSEEAoscope_TOP | CURSOR2/spriteBlank_reg                                           | 7      | 1     | NO           | NO                 | YES               | 1      | 0       | 
2default:defaulth p
x
� 
�
%s
*synth2�
�+----------------+-------------------------------------------------------------------+--------+-------+--------------+--------------------+-------------------+--------+---------+

2default:defaulth p
x
� 
�
%s
*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
O
%s
*synth27
#+------+--------------+----------+
2default:defaulth p
x
� 
O
%s
*synth27
#|      |BlackBox name |Instances |
2default:defaulth p
x
� 
O
%s
*synth27
#+------+--------------+----------+
2default:defaulth p
x
� 
O
%s
*synth27
#|1     |CharactersROM |         2|
2default:defaulth p
x
� 
O
%s
*synth27
#|2     |clk_wiz_0     |         1|
2default:defaulth p
x
� 
O
%s
*synth27
#|3     |blk_mem_gen_0 |         8|
2default:defaulth p
x
� 
O
%s
*synth27
#+------+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
O
%s*synth27
#+------+------------------+------+
2default:defaulth px� 
O
%s*synth27
#|      |Cell              |Count |
2default:defaulth px� 
O
%s*synth27
#+------+------------------+------+
2default:defaulth px� 
O
%s*synth27
#|1     |CharactersROM     |     1|
2default:defaulth px� 
O
%s*synth27
#|2     |CharactersROM__1  |     1|
2default:defaulth px� 
O
%s*synth27
#|3     |blk_mem_gen_0     |     1|
2default:defaulth px� 
O
%s*synth27
#|4     |blk_mem_gen_0__10 |     1|
2default:defaulth px� 
O
%s*synth27
#|5     |blk_mem_gen_0__4  |     1|
2default:defaulth px� 
O
%s*synth27
#|6     |blk_mem_gen_0__5  |     1|
2default:defaulth px� 
O
%s*synth27
#|7     |blk_mem_gen_0__6  |     1|
2default:defaulth px� 
O
%s*synth27
#|8     |blk_mem_gen_0__7  |     1|
2default:defaulth px� 
O
%s*synth27
#|9     |blk_mem_gen_0__8  |     1|
2default:defaulth px� 
O
%s*synth27
#|10    |blk_mem_gen_0__9  |     1|
2default:defaulth px� 
O
%s*synth27
#|11    |clk_wiz_0         |     1|
2default:defaulth px� 
O
%s*synth27
#|12    |CARRY4            |  2792|
2default:defaulth px� 
O
%s*synth27
#|13    |DSP48E1           |     2|
2default:defaulth px� 
O
%s*synth27
#|14    |DSP48E1_10        |     1|
2default:defaulth px� 
O
%s*synth27
#|15    |DSP48E1_6         |     2|
2default:defaulth px� 
O
%s*synth27
#|16    |DSP48E1_7         |     4|
2default:defaulth px� 
O
%s*synth27
#|17    |DSP48E1_8         |     2|
2default:defaulth px� 
O
%s*synth27
#|18    |DSP48E1_9         |     1|
2default:defaulth px� 
O
%s*synth27
#|19    |LUT1              |   775|
2default:defaulth px� 
O
%s*synth27
#|20    |LUT2              |  3665|
2default:defaulth px� 
O
%s*synth27
#|21    |LUT3              |   960|
2default:defaulth px� 
O
%s*synth27
#|22    |LUT4              |  6601|
2default:defaulth px� 
O
%s*synth27
#|23    |LUT5              |   512|
2default:defaulth px� 
O
%s*synth27
#|24    |LUT6              |  1851|
2default:defaulth px� 
O
%s*synth27
#|25    |MUXF7             |    46|
2default:defaulth px� 
O
%s*synth27
#|26    |MUXF8             |    18|
2default:defaulth px� 
O
%s*synth27
#|27    |SRL16E            |   162|
2default:defaulth px� 
O
%s*synth27
#|28    |XADC              |     1|
2default:defaulth px� 
O
%s*synth27
#|29    |FDRE              |  1729|
2default:defaulth px� 
O
%s*synth27
#|30    |FDSE              |    10|
2default:defaulth px� 
O
%s*synth27
#|31    |IBUF              |    13|
2default:defaulth px� 
O
%s*synth27
#|32    |OBUF              |    15|
2default:defaulth px� 
O
%s*synth27
#+------+------------------+------+
2default:defaulth px� 
E
%s
*synth2-

Report Instance Areas: 
2default:defaulth p
x
� 
z
%s
*synth2b
N+------+---------------------+---------------------------------------+------+
2default:defaulth p
x
� 
z
%s
*synth2b
N|      |Instance             |Module                                 |Cells |
2default:defaulth p
x
� 
z
%s
*synth2b
N+------+---------------------+---------------------------------------+------+
2default:defaulth p
x
� 
z
%s
*synth2b
N|1     |top                  |                                       | 19360|
2default:defaulth p
x
� 
z
%s
*synth2b
N|2     |  TEXT               |Text                                   |  8571|
2default:defaulth p
x
� 
z
%s
*synth2b
N|3     |  BIN2DEC_TIMEDIV    |binary2decimal                         |    47|
2default:defaulth p
x
� 
z
%s
*synth2b
N|4     |  ADCC               |adc_controller                         |   106|
2default:defaulth p
x
� 
z
%s
*synth2b
N|5     |  AUTO               |debounce                               |    35|
2default:defaulth p
x
� 
z
%s
*synth2b
N|6     |  BUFF_CH1           |buffer__xdcDup__1                      |   183|
2default:defaulth p
x
� 
z
%s
*synth2b
N|7     |  BUFF_CH1B          |buffer__xdcDup__2                      |   168|
2default:defaulth p
x
� 
z
%s
*synth2b
N|8     |  BUFF_CH2           |buffer__xdcDup__3                      |   184|
2default:defaulth p
x
� 
z
%s
*synth2b
N|9     |  BUFF_CH2B          |buffer                                 |   168|
2default:defaulth p
x
� 
z
%s
*synth2b
N|10    |  BUFF_SEL           |buffer_select                          |     9|
2default:defaulth p
x
� 
z
%s
*synth2b
N|11    |  CENTER             |debounce_0                             |    35|
2default:defaulth p
x
� 
z
%s
*synth2b
N|12    |  CH_TOGGLE          |channel_toggle                         |    54|
2default:defaulth p
x
� 
z
%s
*synth2b
N|13    |  CURSOR1            |draw_horizontal_line__parameterized0   |    71|
2default:defaulth p
x
� 
z
%s
*synth2b
N|14    |  CURSOR1_TOTEXT     |cursor_text                            |   180|
2default:defaulth p
x
� 
z
%s
*synth2b
N|15    |    BIN2DEC          |binary2decimal_14                      |    52|
2default:defaulth p
x
� 
z
%s
*synth2b
N|16    |    CURSOR2VOLT      |cursor_to_voltage_15                   |   128|
2default:defaulth p
x
� 
z
%s
*synth2b
N|17    |  CURSOR2            |draw_horizontal_line__parameterized0_1 |    47|
2default:defaulth p
x
� 
z
%s
*synth2b
N|18    |  CURSOR2_TOTEXT     |cursor_text_2                          |   183|
2default:defaulth p
x
� 
z
%s
*synth2b
N|19    |    BIN2DEC          |binary2decimal_12                      |    55|
2default:defaulth p
x
� 
z
%s
*synth2b
N|20    |    CURSOR2VOLT      |cursor_to_voltage_13                   |   128|
2default:defaulth p
x
� 
z
%s
*synth2b
N|21    |  DOWN               |debounce_3                             |    54|
2default:defaulth p
x
� 
z
%s
*synth2b
N|22    |  DRAW_TRIGGER       |draw_horizontal_line                   |    88|
2default:defaulth p
x
� 
z
%s
*synth2b
N|23    |  DRAW_WAVE_CH1      |draw_wave                              |   162|
2default:defaulth p
x
� 
z
%s
*synth2b
N|24    |  DRAW_WAVE_CH2      |draw_wave__parameterized0              |   124|
2default:defaulth p
x
� 
z
%s
*synth2b
N|25    |  FILTER             |low_pass_filter                        |   188|
2default:defaulth p
x
� 
z
%s
*synth2b
N|26    |  FILTER2            |low_pass_filter_4                      |   189|
2default:defaulth p
x
� 
z
%s
*synth2b
N|27    |  GRID               |Grid                                   |    74|
2default:defaulth p
x
� 
z
%s
*synth2b
N|28    |  LOOKUP             |lookup_text                            |  4263|
2default:defaulth p
x
� 
z
%s
*synth2b
N|29    |  MEASURE_SIGNAL_CH1 |measure_signal                         |  1540|
2default:defaulth p
x
� 
z
%s
*synth2b
N|30    |  MEASURE_SIGNAL_CH2 |measure_signal_5                       |  1547|
2default:defaulth p
x
� 
z
%s
*synth2b
N|31    |  OS_AUTO            |one_shot                               |     9|
2default:defaulth p
x
� 
z
%s
*synth2b
N|32    |  OS_CNT             |one_shot_6                             |     2|
2default:defaulth p
x
� 
z
%s
*synth2b
N|33    |  OS_DN              |one_shot_7                             |     2|
2default:defaulth p
x
� 
z
%s
*synth2b
N|34    |  OS_UP              |one_shot_8                             |     5|
2default:defaulth p
x
� 
z
%s
*synth2b
N|35    |  SETTINGS           |settings                               |   251|
2default:defaulth p
x
� 
z
%s
*synth2b
N|36    |  TIME_DIV           |get_timeDiv                            |    74|
2default:defaulth p
x
� 
z
%s
*synth2b
N|37    |  TOGGLE_CHA         |toggle_channel                         |    43|
2default:defaulth p
x
� 
z
%s
*synth2b
N|38    |  TRIGGER            |sw_trigger                             |    48|
2default:defaulth p
x
� 
z
%s
*synth2b
N|39    |  UP                 |debounce_9                             |    83|
2default:defaulth p
x
� 
z
%s
*synth2b
N|40    |  VERT_SCALE         |vertScale_conversion                   |    12|
2default:defaulth p
x
� 
z
%s
*synth2b
N|41    |  VGA_CONTRL         |xvga1280_1024                          |    99|
2default:defaulth p
x
� 
z
%s
*synth2b
N|42    |  VOLTSDIV_TO_TEXT   |cursor_text_10                         |   117|
2default:defaulth p
x
� 
z
%s
*synth2b
N|43    |    BIN2DEC          |binary2decimal_11                      |    51|
2default:defaulth p
x
� 
z
%s
*synth2b
N|44    |    CURSOR2VOLT      |cursor_to_voltage                      |    66|
2default:defaulth p
x
� 
z
%s
*synth2b
N|45    |  XADC               |xadc                                   |     1|
2default:defaulth p
x
� 
z
%s
*synth2b
N+------+---------------------+---------------------------------------+------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:01:16 ; elapsed = 00:01:19 . Memory (MB): peak = 1238.375 ; gain = 892.402
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
t
%s
*synth2\
HSynthesis finished with 0 errors, 98 critical warnings and 42 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
Synthesis Optimization Runtime : Time (s): cpu = 00:01:05 ; elapsed = 00:01:11 . Memory (MB): peak = 1238.375 ; gain = 527.133
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Complete : Time (s): cpu = 00:01:16 ; elapsed = 00:01:19 . Memory (MB): peak = 1238.375 ; gain = 892.402
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
h
-Analyzing %s Unisim elements for replacement
17*netlist2
28822default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
3782default:default2
1682default:default2
982default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2"
synth_design: 2default:default2
00:01:212default:default2
00:01:242default:default2
1238.3752default:default2
903.9182default:defaultZ17-268h px� 
K
"No constraint will be written out.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2f
RC:/temp/DEMOS/oseeocope/OSEEAoscope/demo05/demo03.runs/synth_1/OSEEAoscope_TOP.dcp2default:defaultZ17-1381h px� 
�
%s4*runtcl2�
tExecuting : report_utilization -file OSEEAoscope_TOP_utilization_synth.rpt -pb OSEEAoscope_TOP_utilization_synth.pb
2default:defaulth px� 
�
treport_utilization: Time (s): cpu = 00:00:00 ; elapsed = 00:00:00.123 . Memory (MB): peak = 1238.375 ; gain = 0.000
*commonh px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Sat Dec  5 21:44:39 20202default:defaultZ17-206h px� 


End Record