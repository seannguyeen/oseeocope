`timescale 1ns / 1ps

module OSEEAoscope_TOP(
   input clk,
   input reset,
   input up_btn, 
   input dn_btn,
   input autoset_btn,
   input [4:0] SW,
   input vauxp11,
   input vauxn11,
   input vauxp3,
   input vauxn3,

   output reg VGA_HS,
   output reg VGA_VS,
   output reg [3:0] VGA_R, VGA_G, VGA_B,
   output wire LED_15);
    
   // PARAMETER DEF
    parameter ADDRESS_BITS = 12;
    parameter   REAL_DISPLAY_WIDTH = 1688;
    parameter REAL_DISPLAY_HEIGHT = 1066; 
    parameter SAMPLE_BITS = 12;
    parameter TOGGLE_CHANNELS_STATE_BITS = 2;
    parameter DISPLAY_X_BITS = 12;
    parameter DISPLAY_Y_BITS = 12;
    parameter  RGB_BITS = 12;   
    parameter SCALE_EXPONENT_BITS = 4; 
    parameter SCALE_FACTOR_BITS = 10;
    parameter SELECT_CHARACTER_BITS = 7;
    parameter GREEN = 12'h0C0;
    parameter LIGHT_PURPLE = 12'hF6F;
  
    // CLK WIZ 
    wire CLK108MHZ;
    wire locked;
 
    clk_wiz_0 clk_wiz_vga (.clk_in1(clk),      
                           .clk_out1(CLK108MHZ),    
                           .reset(0),
                           .locked(locked));      
    
    // DEBOUNCE ALL MODULES 
    wire rst, db_up, db_dn, db_autoset;
    
    debounce CENTER(.reset(0),
                    .clk(CLK108MHZ), 
                    .noisy(reset), 
                    .clean(rst)), 
                                      
             UP (.clk(CLK108MHZ), 
                 .reset(0),
                 .noisy(up_btn), 
                 .clean(db_up)),
             
             DOWN (.clk(CLK108MHZ), 
                   .reset(0),
                   .noisy(dn_btn), 
                   .clean(db_dn)),
                   
            AUTO (.clk(CLK108MHZ), 
                  .reset(0),
                  .noisy(autoset_btn), 
                  .clean(db_autoset));
   
   // ONE SHOTS 
   wire btnc_pulse, btnup_pulse, btndn_pulse, btnauto_pulse; 
   
   one_shot OS_CNT(.clk(CLK108MHZ), 
                   .btn(rst), 
                   .btnpulse(btnc_pulse)), 
                   
            OS_UP (.clk(CLK108MHZ), 
                   .btn(db_up), 
                   .btnpulse(btnup_pulse)),        
   
            OS_DN (.clk(CLK108MHZ), 
                   .btn(db_dn), 
                   .btnpulse(btndn_pulse)),
           
           OS_AUTO (.clk(CLK108MHZ), 
                    .btn(db_autoset), 
                    .btnpulse(btnauto_pulse));
   
    // SETTINGS
    wire  signed [11:0] triggerThreshold;
    wire [9:0] verticalScaleFactorTimes8Channel1;
    wire [9:0] verticalScaleFactorTimes8Channel2;
    wire [5:0] samplePeriod;
    wire channelSelected;
    wire signed [DISPLAY_Y_BITS-1:0] yCursor1;
    wire signed [DISPLAY_Y_BITS-1:0] yCursor2;
    assign LED_15 = channelSelected; 
 
    // measure_signal wires 
    wire signed [11:0] signalMinChannel1;
    wire signed [11:0] signalMaxChannel1;
    wire [11:0] signalPeriodChannel1;
    wire signed [11:0] signalAverageChannel1;
    wire signed [11:0] signalMinChannel2;
    wire signed [11:0] signalMaxChannel2;
    wire [11:0] signalPeriodChannel2;
    wire signed [11:0] signalAverageChannel2;
    
    settings SETTINGS(.clock(CLK108MHZ), 
                      .sw(SW),
                      .btnu(btnup_pulse), 
                      .btnd(btndn_pulse), 
                      .btnc(btnc_pulse), 
                      .btnl(btnauto_pulse),
                      .buttonUpClean(db_up), 
                      .buttonDownClean(db_dn),
                      .signalMinChannel1(signalMinChannel1), 
                      .signalMaxChannel1(signalMaxChannel1), 
                      .signalPeriodChannel1(signalPeriodChannel1),
                      .signalMinChannel2(signalMinChannel2), 
                      .signalMaxChannel2(signalMaxChannel2), 
                      .signalPeriodChannel2(signalPeriodChannel2),
                      .triggerThreshold(triggerThreshold), 
                      .verticalScaleFactorTimes8Channel1(verticalScaleFactorTimes8Channel1), 
                      .verticalScaleFactorTimes8Channel2(verticalScaleFactorTimes8Channel2),
                      .samplePeriod(samplePeriod), 
                      .channelSelected(channelSelected),
                      .yCursor1(yCursor1),
                      .yCursor2(yCursor2));
    
    // VERTICAL SCALE CONV
    wire [3:0] verticalScaleExponentChannel1;
    wire [3:0] verticalScaleExponentChannel2;
    vertScale_conversion VERT_SCALE (.clock(CLK108MHZ),
                                     .verticalScaleFactorTimes8Channel1(verticalScaleFactorTimes8Channel1),
                                     .verticalScaleFactorTimes8Channel2(verticalScaleFactorTimes8Channel2),
                                     .verticalScaleExponentChannel1(verticalScaleExponentChannel1),
                                     .verticalScaleExponentChannel2(verticalScaleExponentChannel2));
    
    // XADC     
    wire ready;
    wire signed [15:0] xadc_dataOut;   
    wire signed [SAMPLE_BITS-1:0] channel1;
    wire signed [SAMPLE_BITS-1:0] channel2;
    wire channelDataReady;
    wire [TOGGLE_CHANNELS_STATE_BITS-1:0] state;
    wire [TOGGLE_CHANNELS_STATE_BITS-1:0] previousState;
    wire endOfConversion,
         DRPEnable,
         DRPWriteEnable;
    wire [7:0] DRPAddress;  
            
    xadc       XADC (.daddr_in(DRPAddress), //addresses can be found in the artix 7 XADC user guide DRP register space
                     .dclk_in(CLK108MHZ), 
                     .den_in(DRPEnable), 
                     .di_in(), 
                     .dwe_in(DRPWriteEnable), 
                     .busy_out(),                    
                     .vauxp3(vauxp3),
                     .vauxn3(vauxn3),
                     .vauxp11(vauxp11),
                     .vauxn11(vauxn11),
                     .vn_in(), 
                     .vp_in(), 
                     .alarm_out(), 
                     .do_out(xadc_dataOut), 
                     .reset_in(0),
                     .eoc_out(endOfConversion),
                     .channel_out(),
                     .drdy_out(ready));
                     
    // TOGGLE SELECTION (RAW VS BUFFER)
    toggle_channel TOGGLE_CHA(.clock(CLK108MHZ),
                              .endOfConversion(endOfConversion),
                              .DRPReady(ready),
                              .DRPDataOut(xadc_dataOut[15:4]),
                              .DRPEnable(DRPEnable),
                              .DRPWriteEnable(DRPWriteEnable),
                              .channel1(channel1),
                              .channel2(channel2), //will use once we get ch1 working 
                              .DRPAddress(DRPAddress),
                              .channelDataReady(channelDataReady),
                              .state(state),
                              .previousState(previousState));
    
    // ADC CONTROLLER
    wire adcc_ready;
    wire adccRawReady;
    wire signed [11:0] ADCCdataOutChannel1;
    wire signed [11:0] adccRawDataOutChannel1;
    wire signed [11:0] ADCCdataOutChannel2;
    wire signed [11:0] adccRawDataOutChannel2;
    
    adc_controller ADCC(.clock(CLK108MHZ),
                        .reset(0),
                        .sampleEnabled(1),
                        .inputReady(channelDataReady),
                        .samplePeriod(samplePeriod), // to be added on later with settings 
                        .ready(adcc_ready),
                        .rawReady(adccRawReady), // not affected by samplePeriod
                        .dataInChannel1(channel1),
                        .dataOutChannel1(ADCCdataOutChannel1),
                        .rawDataOutChannel1(adccRawDataOutChannel1),
                        .dataInChannel2(channel2), // no ch2 yet
                        .dataOutChannel2(ADCCdataOutChannel2), // no ch2 yet
                        .rawDataOutChannel2(adccRawDataOutChannel2)); // no ch2 yet
    
    // LOW PASS FILTER
    wire risingEdgeReadyChannel1;
    wire signed [13:0] slopeChannel1;
    wire positiveSlopeChannel1;
    low_pass_filter FILTER(.clock(CLK108MHZ),
                           .dataReady(adccRawReady),
                           .dataIn(adccRawDataOutChannel1),
                           .risingEdgeReady(risingEdgeReadyChannel1),
                           .estimatedSlope(slopeChannel1),
                           .estimatedSlopeIsPositive(positiveSlopeChannel1));
    
    // LOW PASS FILTER CH02
    wire risingEdgeReadyChannel2;
    wire signed [13:0] slopeChannel2;
    wire positiveSlopeChannel2;
    low_pass_filter FILTER2 (.clock(CLK108MHZ),
                             .dataReady(adccRawReady),
                             .dataIn(adccRawDataOutChannel2),
                             .risingEdgeReady(risingEdgeReadyChannel2),
                             .estimatedSlope(slopeChannel2),
                             .estimatedSlopeIsPositive(positiveSlopeChannel2));
    
    // BUFFER SELECTION
    wire [ADDRESS_BITS-1:0] curveAddressOutChannel1;
    wire [ADDRESS_BITS-1:0] curveAddressOutChannel2;
    wire isTriggered;
    wire [11:0] bufferDataOutChannel1;
    wire drawStarting; 
    wire activeBramSelect;
    
    buffer_select BUFF_SEL (.clock(CLK108MHZ),
                            .drawStarting(drawStarting),
                            .activeBramSelect(activeBramSelect));
   // BUFFER CH1     
    buffer BUFF_CH1 (.clock(CLK108MHZ), 
                     .ready(adcc_ready), 
                     .dataIn(ADCCdataOutChannel1), 
                     .isTrigger(isTriggered), 
                     .disableCollection(0), 
                     .activeBramSelect(activeBramSelect),
                     .reset(0),
                     .readTriggerRelative(1),
                     .readAddress(curveAddressOutChannel1),
                     .dataOut(bufferDataOutChannel1));
                        
    wire [11:0]  buffer2DataOutChannel1;
    wire [ADDRESS_BITS-1:0] curveAddressOut2Channel1; 
    
    buffer BUFF_CH1B (.clock(CLK108MHZ), 
                     .ready(risingEdgeReadyChannel1), 
                     .dataIn(slopeChannel1),
                     .isTrigger(isTriggered), 
                     .disableCollection(0), 
                     .activeBramSelect(activeBramSelect),
                     .reset(0),
                     .readTriggerRelative(1),
                     .readAddress(curveAddressOut2Channel1),
                     .dataOut(buffer2DataOutChannel1));
    
    // BUFFER CH2
    wire [11:0] bufferDataOutChannel2;      

    buffer BUFF_CH2 (.clock(CLK108MHZ), 
                     .ready(adcc_ready), 
                     .dataIn(ADCCdataOutChannel2), 
                     .isTrigger(isTriggered), 
                     .disableCollection(0), 
                     .activeBramSelect(activeBramSelect),
                     .reset(0),
                     .readTriggerRelative(1),
                     .readAddress(curveAddressOutChannel2),
                     .dataOut(bufferDataOutChannel2));
                                            
    wire [11:0]  buffer2DataOutChannel2;
    wire [ADDRESS_BITS-1:0] curveAddressOut2Channel2; 
    
    buffer BUFF_CH2B (.clock(CLK108MHZ), 
                     .ready(risingEdgeReadyChannel2), 
                     .dataIn(slopeChannel2),
                     .isTrigger(isTriggered), 
                     .disableCollection(0), 
                     .activeBramSelect(activeBramSelect),
                     .reset(0),
                     .readTriggerRelative(1),
                     .readAddress(curveAddressOut2Channel2),
                     .dataOut(buffer2DataOutChannel2));
    
    // SOFTWARE TRIGGER 
    wire [SAMPLE_BITS-1:0] channelSelectedData;
    wire positiveSlopeChannelSelected;
    wire [SCALE_FACTOR_BITS-1:0] verticalScaleFactorTimes8ChannelSelected;
    wire [SCALE_EXPONENT_BITS-1:0] verticalScaleExponentChannelSelected;
    
    channel_toggle CH_TOGGLE
                (.clock(CLK108MHZ),
                .channel1(adccRawDataOutChannel1),
                .channel2(adccRawDataOutChannel2), // NO VALUE YET! 
                .positiveSlopeChannel1(positiveSlopeChannel1),
                .positiveSlopeChannel2(positiveSlopeChannel2), // NO VALUE YET!
                .verticalScaleFactorTimes8Channel1(verticalScaleFactorTimes8Channel1),
                .verticalScaleFactorTimes8Channel2(verticalScaleFactorTimes8Channel2), // NO VALUE YET!
                .verticalScaleExponentChannel1(verticalScaleExponentChannel1),
                .verticalScaleExponentChannel2(verticalScaleExponentChannel2), // NO VALUE YET!
                .channelSelected(channelSelected),
                .channelSelectedData(channelSelectedData),
                .positiveSlopeChannelSelected(positiveSlopeChannelSelected),
                .verticalScaleFactorTimes8ChannelSelected(verticalScaleFactorTimes8ChannelSelected),
                .verticalScaleExponentChannelSelected(verticalScaleExponentChannelSelected)
                );
            
        sw_trigger #(.DATA_BITS(12))
                TRIGGER
                (.clock(CLK108MHZ),
                .threshold(triggerThreshold),
                .dataReady(adccRawReady),
                .dataIn(channelSelectedData-12'h777),
                .triggerDisable(~positiveSlopeChannelSelected),
                .isTriggered(isTriggered)
                );
    
    // MEASURE SIGNAL 
    measure_signal MEASURE_SIGNAL_CH1(.clock(CLK108MHZ),
                                      .dataReady(adccRawReady),
                                      .dataIn(adccRawDataOutChannel1),
                                      .isTrigger(isTriggered),
                                      .signalMax(signalMaxChannel1),
                                      .signalMin(signalMinChannel1),
                                      .signalPeriod(signalPeriodChannel1),
                                      .signalAverage(signalAverageChannel1));
    
    measure_signal MEASURE_SIGNAL_CH2(.clock(CLK108MHZ),
                                      .dataReady(adccRawReady),
                                      .dataIn(adccRawDataOutChannel2),
                                      .isTrigger(isTriggered),
                                      .signalMax(signalMaxChannel2),
                                      .signalMin(signalMinChannel2),
                                      .signalPeriod(signalPeriodChannel2),
                                      .signalAverage(signalAverageChannel2));                                  
      
    // VGA                                    
    wire [11:0] displayX;
    wire [11:0] displayY;
    wire [3:0] vga_r;
    wire [3:0] vga_g;
    wire [3:0] vga_b;
    wire vsync;
    wire hsync;
    wire blank;

    xvga1280_1024 VGA_CONTRL(.vclock(CLK108MHZ),
                             .displayX(displayX), // pixel number on current line
                             .displayY(displayY), // line number
                             .vsync(vsync),
                             .hsync(hsync),
                             .blank(blank));
   
   // GRID 
    wire [11:0] gridDisplayX,
                gridDisplayY, 
                gridPixel; 
    wire gridHsync, 
         gridVsync,
         gridBlank; 
        
    Grid GRID(.clock(CLK108MHZ), 
                .displayX(displayX), 
                .displayY(displayY), 
                .hsync(hsync), 
                .vsync(vsync), 
                .blank(blank),
                .gridDisplayX(gridDisplayX), 
                .gridDisplayY(gridDisplayY), 
                .gridHsync(gridHsync), 
                .gridVsync(gridVsync), 
                .gridBlank(gridBlank), 
                .pixel(gridPixel));
    
    // DRAW WAVE
    // CHANNEL 1 COLOR : YELLOW VAUX3
    // CHANNEL 2 COLOR: BLUE VAUX11
    wire [11:0] dataInChannel1;
    wire [DISPLAY_X_BITS-1:0] curveChannel1DisplayX;
    wire [DISPLAY_Y_BITS-1:0] curveChannel1DisplayY;
    wire curveChannel1Hsync;
    wire curveChannel1Vsync;
    wire curveChannel1Blank;
    wire [RGB_BITS-1:0] curveChannel1Pixel;
    

    assign dataInChannel1 = bufferDataOutChannel1;
    draw_wave #(.ADDRESS_BITS(ADDRESS_BITS))
            DRAW_WAVE_CH1
            (.clock(CLK108MHZ),
            .dataIn(dataInChannel1),
            .verticalScaleFactorTimes8(verticalScaleFactorTimes8Channel1), 
            .displayX(gridDisplayX),
            .displayY(gridDisplayY),
            .hsync(gridHsync),
            .vsync(gridVsync),
            .blank(gridBlank),
            .previousPixel(gridPixel),
            .pixel(curveChannel1Pixel),
            .drawStarting(), // Not ties to any logic 
            .address(curveAddressOutChannel1),
            .curveDisplayX(curveChannel1DisplayX),
            .curveDisplayY(curveChannel1DisplayY),
            .curveHsync(curveChannel1Hsync),
            .curveVsync(curveChannel1Vsync),
            .curveBlank(curveChannel1Blank)
            );    
    
    wire [11:0] dataInChannel2;
    wire [DISPLAY_X_BITS-1:0] curveChannel2DisplayX;
    wire [DISPLAY_Y_BITS-1:0] curveChannel2DisplayY;
    wire curveChannel2Hsync;
    wire curveChannel2Vsync;
    wire curveChannel2Blank;
    wire [RGB_BITS-1:0] curveChannel2Pixel;
    
    
    assign dataInChannel2 = bufferDataOutChannel2;
    draw_wave #(.ADDRESS_BITS(ADDRESS_BITS),
            .RGB_COLOR(12'h0FF)) //blue
            DRAW_WAVE_CH2
            (.clock(CLK108MHZ),
            .dataIn(dataInChannel2),
            .verticalScaleFactorTimes8(verticalScaleFactorTimes8Channel2), 
            .displayX(curveChannel1DisplayX),
            .displayY(curveChannel1DisplayY),
            .hsync(curveChannel1Hsync),
            .vsync(curveChannel1Vsync),
            .blank(curveChannel1Blank),
            .previousPixel(curveChannel1Pixel),
            .pixel(curveChannel2Pixel),
            .drawStarting(), // Not ties to any logic 
            .address(curveAddressOutChannel2),
            .curveDisplayX(curveChannel2DisplayX),
            .curveDisplayY(curveChannel2DisplayY),
            .curveHsync(curveChannel2Hsync),
            .curveVsync(curveChannel2Vsync),
            .curveBlank(curveChannel2Blank)
            );    
    
    // DRAWSTRING SIGNAL (END OF VGA SCAN) 
    assign drawStarting = (gridDisplayX == REAL_DISPLAY_WIDTH - 1) && (gridDisplayY == (REAL_DISPLAY_HEIGHT - 1));
    
    // DRAW TRIGGER HORIZONTAL LINE
    wire [RGB_BITS-1:0] tlsPixel;
    wire [DISPLAY_X_BITS-1:0] tlsDisplayX;
    wire [DISPLAY_Y_BITS-1:0] tlsDisplayY;
    wire tlsHsync, tlsVsync, tlsBlank;
    
    draw_horizontal_line DRAW_TRIGGER (.clock(CLK108MHZ),
                                       .level(triggerThreshold * $signed(verticalScaleFactorTimes8ChannelSelected) / 'sd8),
                                       .displayX(curveChannel2DisplayX),
                                       .displayY(curveChannel2DisplayY),
                                       .hsync(curveChannel2Hsync),
                                       .vsync(curveChannel2Vsync),
                                       .blank(curveChannel2Blank),
                                       .previousPixel(curveChannel2Pixel),
                                       .pixel(tlsPixel),
                                       .spriteDisplayX(tlsDisplayX),
                                       .spriteDisplayY(tlsDisplayY),
                                       .spriteHsync(tlsHsync),
                                       .spriteVsync(tlsVsync),
                                       .spriteBlank(tlsBlank)); 
    
    // DRAW CURSOR 1 (GREEN)
    wire [RGB_BITS-1:0] cursor1Pixel;
    wire [DISPLAY_X_BITS-1:0] cursor1DisplayX;
    wire [DISPLAY_Y_BITS-1:0] cursor1DisplayY;
    wire cursor1Hsync, cursor1Vsync, cursor1Blank;
    
    draw_horizontal_line #(.RGB_COLOR(GREEN), .ADDITIONAL_LINE_PIXELS(1'b0)) 
    CURSOR1 (.clock(CLK108MHZ),
             .level(yCursor1),
             .displayX(tlsDisplayX),
             .displayY(tlsDisplayY),
             .hsync(tlsHsync),
             .vsync(tlsVsync),
             .blank(tlsBlank),
             .previousPixel(tlsPixel),
             .pixel(cursor1Pixel),
             .spriteDisplayX(cursor1DisplayX),
             .spriteDisplayY(cursor1DisplayY),
             .spriteHsync(cursor1Hsync),
             .spriteVsync(cursor1Vsync),
             .spriteBlank(cursor1Blank));

    // CURSOR 1 TEXT 
    wire [6:0] cursor1Character2, cursor1Character3, cursor1Character4;
    wire cursor1IsPositive, cursor2IsPositive;
    
    cursor_text CURSOR1_TOTEXT (.clock(CLK108MHZ), 
                                .signal(yCursor1), 
                                .scaleExponent(verticalScaleExponentChannelSelected), 
                                .scale(verticalScaleFactorTimes8ChannelSelected), 
                                .character2(cursor1Character4), 
                                .character1(cursor1Character3), 
                                .character0(cursor1Character2),
                                .isPositive(cursor1IsPositive)); 
    
    // CURSOR 2 TEXT 
    wire [6:0] cursor2Character2, cursor2Character3, cursor2Character4;
        
    cursor_text CURSOR2_TOTEXT (.clock(CLK108MHZ), 
                                .signal(yCursor2), 
                                .scaleExponent(verticalScaleExponentChannelSelected), 
                                .scale(verticalScaleFactorTimes8ChannelSelected), 
                                .character2(cursor2Character4), 
                                .character1(cursor2Character3), 
                                .character0(cursor2Character2),
                                .isPositive(cursor2IsPositive)); 
        
    // DRAW CURSOR 2 (GREEN)
     wire [RGB_BITS-1:0] cursor2Pixel;
     wire [DISPLAY_X_BITS-1:0] cursor2DisplayX;
     wire [DISPLAY_Y_BITS-1:0] cursor2DisplayY;
     wire cursor2Hsync, cursor2Vsync, cursor2Blank;
     
     draw_horizontal_line #(.RGB_COLOR(GREEN), .ADDITIONAL_LINE_PIXELS(1'b0)) 
     CURSOR2 (.clock(CLK108MHZ),
              .level(yCursor2),
              .displayX(cursor1DisplayX),
              .displayY(cursor1DisplayY),
              .hsync(cursor1Hsync),
              .vsync(cursor1Vsync),
              .blank(cursor1Blank),
              .previousPixel(cursor1Pixel),
              .pixel(cursor2Pixel),
              .spriteDisplayX(cursor2DisplayX),
              .spriteDisplayY(cursor2DisplayY),
              .spriteHsync(cursor2Hsync),
              .spriteVsync(cursor2Vsync),
              .spriteBlank(cursor2Blank));
     
    // TIME/DIV TEXT 
    wire [9:0] timePerDivision;  
    get_timeDiv TIME_DIV
            (.clock(CLK108MHZ),
            .samplePeriod(samplePeriod),
            .timePerDivision(timePerDivision));
    
    wire [6:0] timePerDivisionNumber0;     
    wire [6:0] timePerDivisionNumber1;
    wire [6:0] timePerDivisionNumber2;   
      
    binary2decimal BIN2DEC_TIMEDIV (.clock(CLK108MHZ),
                                    .data(timePerDivision),
                                    .character0(timePerDivisionNumber0),
                                    .character1(timePerDivisionNumber1),
                                    .character2(timePerDivisionNumber2));
    
    // VOLT/DIV TEXT
    wire [6:0] voltagePerDivisionCharacter4,
               voltagePerDivisionCharacter3,
               voltagePerDivisionCharacter2;
    
     cursor_text VOLTSDIV_TO_TEXT(.clock(CLK108MHZ), 
                                   .signal(8'sd100), 
                                   .scaleExponent(verticalScaleExponentChannelSelected), 
                                   .scale(verticalScaleFactorTimes8ChannelSelected), 
                                   .character2(voltagePerDivisionCharacter4), 
                                   .character1(voltagePerDivisionCharacter3), 
                                   .character0(voltagePerDivisionCharacter2),
                                   .isPositive()); 
     
    // TEXT 
    wire textHsync, textVsync, textBlank;
    wire [RGB_BITS-1:0] textPixel;
    wire [DISPLAY_X_BITS-1:0] textDisplayX;
    wire [DISPLAY_Y_BITS-1:0] textDisplayY;
    wire [SELECT_CHARACTER_BITS-1:0] middleVoltageCharacter4;
    assign middleVoltageCharacter4 = 7'd0;   //space
    wire [SELECT_CHARACTER_BITS-1:0] middleVoltageCharacter3;
    assign middleVoltageCharacter3 = 7'd0;   //space
    wire [SELECT_CHARACTER_BITS-1:0] middleVoltageCharacter2;
    assign middleVoltageCharacter2 = 7'd16;  //0
    wire [SELECT_CHARACTER_BITS-1:0] middleVoltageCharacter1;
    assign middleVoltageCharacter1 = 7'd77;  //m
    wire [SELECT_CHARACTER_BITS-1:0] middleVoltageCharacter0;
    assign middleVoltageCharacter0 = 7'd54;  //V
    
    wire [SELECT_CHARACTER_BITS-1:0] timePerDivisionCharacter1;
    assign timePerDivisionCharacter1 = 7'd77;  //m
    wire [SELECT_CHARACTER_BITS-1:0] timePerDivisionCharacter0;
    assign timePerDivisionCharacter0 = 7'd83;  //s
    
    wire [SELECT_CHARACTER_BITS-1:0] voltagePerDivisionCharacter1;
    assign voltagePerDivisionCharacter1 = 7'd77;  //m
    wire [SELECT_CHARACTER_BITS-1:0] voltagePerDivisionCharacter0;
    assign voltagePerDivisionCharacter0 = 7'd54;  //V
    
    wire [SELECT_CHARACTER_BITS-1:0] maxCharacter9;
    assign maxCharacter9 = 7'd77;  //m
    wire [SELECT_CHARACTER_BITS-1:0] maxCharacter8;
    assign maxCharacter8 = 7'd65;  //a
    wire [SELECT_CHARACTER_BITS-1:0] maxCharacter7;
    assign maxCharacter7 = 7'd88;  //x
    wire [SELECT_CHARACTER_BITS-1:0] maxCharacter6;
    assign maxCharacter6 = 7'd26;  //:
    wire [SELECT_CHARACTER_BITS-1:0] maxCharacter5;
    assign maxCharacter5 = 1 ? 7'd0 : 7'd13;  //space or -
  
    wire [SELECT_CHARACTER_BITS-1:0] maxCharacter1;
    assign maxCharacter1 = 7'd77;  //m
    wire [SELECT_CHARACTER_BITS-1:0] maxCharacter0;
    assign maxCharacter0 = 7'd54;  //V
    
    wire [SELECT_CHARACTER_BITS-1:0] minCharacter9;
    assign minCharacter9 = 7'd77;  //m
    wire [SELECT_CHARACTER_BITS-1:0] minCharacter8;
    assign minCharacter8 = 7'd73;  //i
    wire [SELECT_CHARACTER_BITS-1:0] minCharacter7;
    assign minCharacter7 = 7'd78;  //n
    wire [SELECT_CHARACTER_BITS-1:0] minCharacter6;
    assign minCharacter6 = 7'd26;  //:
    wire [SELECT_CHARACTER_BITS-1:0] minCharacter5;
    assign minCharacter5 = 1 ? 7'd0 : 7'd13;  //space or -

    wire [SELECT_CHARACTER_BITS-1:0] minCharacter1;
    assign minCharacter1 = 7'd77;  //m
    wire [SELECT_CHARACTER_BITS-1:0] minCharacter0;
    assign minCharacter0 = 7'd54;  //V
    
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character15;
    assign cursor1Character15 = 7'd35;  //C
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character14;
    assign cursor1Character14 = 7'd85;  //u
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character13;
    assign cursor1Character13 = 7'd82;  //r
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character12;
    assign cursor1Character12 = 7'd83;  //s
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character11;
    assign cursor1Character11 = 7'd79;  //o
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character10;
    assign cursor1Character10 = 7'd82;  //r
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character9;
    assign cursor1Character9 = 7'd0;  //Space
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character8;
    assign cursor1Character8 = 7'd17;  //1
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character7;
    assign cursor1Character7 = 7'd26;  //:
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character6;
    assign cursor1Character6 = 7'd0;  //Space
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character5;
    assign cursor1Character5 = ~cursor1IsPositive ? 7'd13 : 7'd0;  //- or space
       
//       wire [SELECT_CHARACTER_BITS-1:0] cursor1Character4;
//       assign cursor1Character4 = cursor1VoltageCharacter2;  //1
//       wire [SELECT_CHARACTER_BITS-1:0] cursor1Character3;
//       assign cursor1Character3 = cursor1VoltageCharacter1;  //2
//       wire [SELECT_CHARACTER_BITS-1:0] cursor1Character2;
//       assign cursor1Character2 = cursor1VoltageCharacter0;  //3
       
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character1;
    assign cursor1Character1 = 7'd77;  //m
    wire [SELECT_CHARACTER_BITS-1:0] cursor1Character0;
    assign cursor1Character0 = 7'd54;  //V
       
    Text TEXT (.clock(CLK108MHZ), 
        .xMiddleVoltage4(1_100), .yMiddleVoltage4(496), .middleVoltageCharacter4(middleVoltageCharacter4),
        .xMiddleVoltage3(1_120), .yMiddleVoltage3(496), .middleVoltageCharacter3(middleVoltageCharacter3),
        .xMiddleVoltage2(1_140), .yMiddleVoltage2(496), .middleVoltageCharacter2(middleVoltageCharacter2),
        .xMiddleVoltage1(1_160), .yMiddleVoltage1(496), .middleVoltageCharacter1(middleVoltageCharacter1),
        .xMiddleVoltage0(1_180), .yMiddleVoltage0(496), .middleVoltageCharacter0(middleVoltageCharacter0),
        
        .xTimePerDivision4(200), .yTimePerDivision4(980), .timePerDivisionCharacter4(timePerDivisionNumber2), // FOR TIME/DIV
        .xTimePerDivision3(220), .yTimePerDivision3(980), .timePerDivisionCharacter3(timePerDivisionNumber1), // FOR TIME DIV
        .xTimePerDivision2(240), .yTimePerDivision2(980), .timePerDivisionCharacter2(timePerDivisionNumber0), // FOR TIME DIV
        .xTimePerDivision1(260), .yTimePerDivision1(980), .timePerDivisionCharacter1(timePerDivisionCharacter1),
        .xTimePerDivision0(280), .yTimePerDivision0(980), .timePerDivisionCharacter0(timePerDivisionCharacter0),
        
        .xVoltagePerDivision4(0),  .yVoltagePerDivision4(744), .voltagePerDivisionCharacter4(voltagePerDivisionCharacter4), // FOR VOLT DIV
        .xVoltagePerDivision3(20), .yVoltagePerDivision3(744), .voltagePerDivisionCharacter3(voltagePerDivisionCharacter3), // FOR VOLT DIV
        .xVoltagePerDivision2(40), .yVoltagePerDivision2(744), .voltagePerDivisionCharacter2(voltagePerDivisionCharacter2), // FOR VOLT DIV
        .xVoltagePerDivision1(60), .yVoltagePerDivision1(744), .voltagePerDivisionCharacter1(voltagePerDivisionCharacter1),
        .xVoltagePerDivision0(80), .yVoltagePerDivision0(744), .voltagePerDivisionCharacter0(voltagePerDivisionCharacter0),
        
        .xMax9(1060), .yMax9(12), .maxCharacter9(maxCharacter9),
        .xMax8(1080), .yMax8(12), .maxCharacter8(maxCharacter8),
        .xMax7(1100), .yMax7(12), .maxCharacter7(maxCharacter7),
        .xMax6(1120), .yMax6(12), .maxCharacter6(maxCharacter6),
        .xMax5(1140), .yMax5(12), .maxCharacter5(maxCharacter5),
        .xMax4(1160), .yMax4(12), .maxCharacter4(), // FOR MAX VAL
        .xMax3(1180), .yMax3(12), .maxCharacter3(), // FOR MAX VAL
        .xMax2(1200), .yMax2(12), .maxCharacter2(), // FOR MAX VAL
        .xMax1(1220), .yMax1(12), .maxCharacter1(maxCharacter1),
        .xMax0(1240), .yMax0(12), .maxCharacter0(maxCharacter0),
               
       .xCursor1_15(940),  .yCursor1_15(948), .cursor1Character15(cursor1Character15),
       .xCursor1_14(960),  .yCursor1_14(948), .cursor1Character14(cursor1Character14),
       .xCursor1_13(980),  .yCursor1_13(948), .cursor1Character13(cursor1Character13),
       .xCursor1_12(1000), .yCursor1_12(948), .cursor1Character12(cursor1Character12),
       .xCursor1_11(1020), .yCursor1_11(948), .cursor1Character11(cursor1Character11),
       .xCursor1_10(1040), .yCursor1_10(948), .cursor1Character10(cursor1Character10),
        .xCursor1_9(1060),  .yCursor1_9(948), .cursor1Character9(cursor1Character9),
        .xCursor1_8(1080),  .yCursor1_8(948), .cursor1Character8(cursor1Character8),
        .xCursor1_7(1100),  .yCursor1_7(948), .cursor1Character7(cursor1Character7),
        .xCursor1_6(1120),  .yCursor1_6(948), .cursor1Character6(cursor1Character6),
        .xCursor1_5(1140),  .yCursor1_5(948), .cursor1Character5(cursor1Character5),
        .xCursor1_4(1160),  .yCursor1_4(948), .cursor1Character4(cursor1Character4),
        .xCursor1_3(1180),  .yCursor1_3(948), .cursor1Character3(cursor1Character3),
        .xCursor1_2(1200),  .yCursor1_2(948), .cursor1Character2(cursor1Character2),
        .xCursor1_1(1220),  .yCursor1_1(948), .cursor1Character1(cursor1Character1),
        .xCursor1_0(1240),  .yCursor1_0(948), .cursor1Character0(cursor1Character0),
       
        .displayX(cursor2DisplayX), .displayY(cursor2DisplayY), 
        .hsync(cursor2Hsync), .vsync(cursor2Vsync), .blank(cursor2Blank), .previousPixel(cursor2Pixel),
        .displayXOut(textDisplayX), .displayYOut(textDisplayY), 
        .hsyncOut(textHsync), .vsyncOut(textVsync), .blankOut(textBlank), .pixel(textPixel), .addressA());
    
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character15;
    assign cursor2Character15 = 7'd35;  //C
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character14;
    assign cursor2Character14 = 7'd85;  //u
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character13;
    assign cursor2Character13 = 7'd82;  //r
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character12;
    assign cursor2Character12 = 7'd83;  //s
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character11;
    assign cursor2Character11 = 7'd79;  //o
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character10;
    assign cursor2Character10 = 7'd82;  //r
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character9;
    assign cursor2Character9 = 7'd0;  //Space
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character8;
    assign cursor2Character8 = 7'd18;  //2
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character7;
    assign cursor2Character7 = 7'd26;  //:
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character6;
    assign cursor2Character6 = 7'd0;  //Space
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character5;
    assign cursor2Character5 = cursor2IsPositive ? 7'd0 : 7'd13;  //space or -
   //     wire [SELECT_CHARACTER_BITS-1:0] cursor2Character4;
   //     assign cursor2Character4 = cursor1VoltageCharacter2;  //1
   //     wire [SELECT_CHARACTER_BITS-1:0] cursor2Character3;
   //     assign cursor2Character3 = cursor1VoltageCharacter1;  //2
   //     wire [SELECT_CHARACTER_BITS-1:0] cursor2Character2;
   //     assign cursor2Character2 = cursor1VoltageCharacter0;  //3
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character1;
    assign cursor2Character1 = 7'd77;  //m
    wire [SELECT_CHARACTER_BITS-1:0] cursor2Character0;
    assign cursor2Character0 = 7'd54;  //V
    
    wire text2Hsync, text2Vsync, text2Blank;
    wire [RGB_BITS-1:0] text2Pixel;
    wire [DISPLAY_X_BITS-1:0] text2DisplayX;
    wire [DISPLAY_Y_BITS-1:0] text2DisplayY;
    
    lookup_text LOOKUP (.clock(CLK108MHZ), 
           .xCharacter29(1060), .yCharacter29(44), .character29(minCharacter9), .character29Color(GREEN),
           .xCharacter28(1080), .yCharacter28(44), .character28(minCharacter8), .character28Color(GREEN),
           .xCharacter27(1100), .yCharacter27(44), .character27(minCharacter7), .character27Color(GREEN),
           .xCharacter26(1120), .yCharacter26(44), .character26(minCharacter6), .character26Color(GREEN),
           .xCharacter25(1140), .yCharacter25(44), .character25(minCharacter5), .character25Color(GREEN),
           
           .xCharacter24(1160), .yCharacter24(44), .character24(), .character24Color(GREEN), // MIN TEXT 
           .xCharacter23(1180), .yCharacter23(44), .character23(), .character23Color(GREEN), // MIN TEXT
           .xCharacter22(1200), .yCharacter22(44), .character22(), .character22Color(GREEN), // MIN TEXT
           .xCharacter21(1220), .yCharacter21(44), .character21(minCharacter1), .character21Color(GREEN),
           .xCharacter20(1240), .yCharacter20(44), .character20(minCharacter0), .character20Color(GREEN),
           
           .xCharacter19(940),  .yCharacter19(980), .character19(cursor2Character15), .character19Color(GREEN),
           .xCharacter18(960),  .yCharacter18(980), .character18(cursor2Character14), .character18Color(GREEN),
           .xCharacter17(980),  .yCharacter17(980), .character17(cursor2Character13), .character17Color(GREEN),
           .xCharacter16(1000), .yCharacter16(980), .character16(cursor2Character12), .character16Color(GREEN),
           .xCharacter15(1020), .yCharacter15(980), .character15(cursor2Character11), .character15Color(GREEN),
                                              
           .xCharacter14(1040), .yCharacter14(980), .character14(cursor2Character10), .character14Color(GREEN),    
           .xCharacter13(1060), .yCharacter13(980), .character13(cursor2Character9), .character13Color(GREEN),
           .xCharacter12(1080), .yCharacter12(980), .character12(cursor2Character8), .character12Color(GREEN),
           .xCharacter11(1100), .yCharacter11(980), .character11(cursor2Character7), .character11Color(GREEN),
           .xCharacter10(1120), .yCharacter10(980), .character10(cursor2Character6), .character10Color(GREEN),
          
           .xCharacter9(1140), .yCharacter9(980), .character9(cursor2Character5), .character9Color(GREEN),
           .xCharacter8(1160), .yCharacter8(980), .character8(cursor2Character4), .character8Color(GREEN),
           .xCharacter7(1180), .yCharacter7(980), .character7(cursor2Character3), .character7Color(GREEN),
           .xCharacter6(1200), .yCharacter6(980), .character6(cursor2Character2), .character6Color(GREEN),
           .xCharacter5(1220), .yCharacter5(980), .character5(cursor2Character1), .character5Color(GREEN),
           
           .xCharacter4(1240), .yCharacter4(980), .character4(cursor2Character0), .character4Color(GREEN),
           .xCharacter3(12), .yCharacter3(12), .character3(), .character3Color(), //channel selected
               
           .xCharacter2(), .yCharacter2(), .character2(), .character2Color(),
           .xCharacter1(), .yCharacter1(), .character1(), .character1Color(),
           .xCharacter0(), .yCharacter0(), .character0(), .character0Color(),
           
           .displayX(textDisplayX), .displayY(textDisplayY), 
           .hsync(textHsync), .vsync(textVsync), .blank(textBlank), .previousPixel(textPixel),
           .displayXOut(text2DisplayX), .displayYOut(text2DisplayY), 
           .hsyncOut(text2Hsync), .vsyncOut(text2Vsync), .blankOut(text2Blank), .pixel(text2Pixel), .addressA());
        
    // ASSIGN FINAL VGA PIXELS 
    always @(posedge CLK108MHZ) 
        begin
        {VGA_R, VGA_G, VGA_B} <= !text2Blank ?  text2Pixel: 12'b0;
        VGA_HS <= text2Hsync;
        VGA_VS <= text2Vsync;
        end   
         
endmodule