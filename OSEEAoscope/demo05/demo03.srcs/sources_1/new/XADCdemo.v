`timescale 1ns / 1ps

module XADCdemo(
   input CLK100MHZ,
   output reg VGA_HS,
   output reg VGA_VS,
   output reg [3:0] VGA_R, VGA_G, VGA_B
 );
    
   wire CLK60MHZ;
   wire locked;
   
   clk_wiz_0 instance_name
  (
  // Clock in ports
   .clk_in1(CLK100MHZ),      // input clk_in1
   // Clock out ports
   .clk_out1(CLK60MHZ),     // output clk_out1
   // Status and control signals
   .reset(0), // input reset
   .locked(locked));      // output locked

   wire [11:0] displayX;
   wire [11:0] displayY;
   wire [3:0] vga_r;
   wire [3:0] vga_g;
   wire [3:0] vga_b;
   wire vsync;
   wire hsync;
   wire blank;
   
//   module xvga(input vclock,
//               output reg [10:0] displayX, // pixel number on current line
//               output reg [9:0]  displayY, // line number
//               output reg           vsync,hsync,blank);
   xvga1280_1024 x(.vclock(CLK60MHZ),
                   .displayX(displayX), // pixel number on current line
                   .displayY(displayY), // line number
                   .vsync(vsync),
                   .hsync(hsync),
                   .blank(blank));
   
//   mysprite ms(displayX, displayY, vsync, hsync, blank,
//               vga_r, vga_g, vga_b);
   
//   assign VGA_HS = hsync;
//   assign VGA_VS = vsync;
//   assign VGA_R = vga_r;
//   assign VGA_G = vga_g;
//   assign VGA_B = vga_b;
   
   wire [11:0] gridDisplayX,
               gridDisplayY, 
               gridPixel; 
               
   wire gridHsync, 
        gridVsync,
        gridBlank; 
        
   Grid myGrid(.clock(CLK60MHZ), 
               .displayX(displayX), 
               .displayY(displayY), 
               .hsync(hsync), 
               .vsync(vsync), 
               .blank(blank),
               .gridDisplayX(gridDisplayX), 
               .gridDisplayY(gridDisplayY), 
               .gridHsync(gridHsync), 
               .gridVsync(gridVsync), 
               .gridBlank(gridBlank), 
               .pixel(gridPixel));
    
//    assign VGA_HS = gridHsync;
//    assign VGA_VS = gridVsync;
//    assign VGA_R = gridPixel[11:8];
//    assign VGA_G = gridPixel[7:4];
//    assign VGA_B = gridPixel[3:0] ;
    
    always @(posedge CLK60MHZ) 
        begin
        {VGA_R, VGA_G, VGA_B} <= !gridBlank ? gridPixel : 12'b0;
        VGA_HS <= gridHsync;
        VGA_VS <= gridVsync;
        end

endmodule
