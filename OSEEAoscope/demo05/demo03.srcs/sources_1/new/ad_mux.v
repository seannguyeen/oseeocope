`timescale 1ns / 1ps
/****************************** C E C S  3 0 1 ******************************
* File Name:	ad_mux.v
* Project:		Lab Project 7: CPU Execution Unit
* Designer: 	Chorro, Edward / Velazquez, Alec
* Email: 		edwardchorro@gmail.com / velazquez_alec@yahoo.com
* Rev. Date: 	April 24, 2018 
* 
* Purpose: 		This module is an 8-to-1 multiplexer used for multiplexing the
*					data to the hex_to_7seg module. The sel variable represents which 
*					anode is ON and the case statement is selecting which data to feed
*					the hex_to7seg module. Data from the register_file and the ALU 
*					is being inputed into this mux. The output will be fed into the 
*					hex_to_7seg modeule. Anodes 7-4 will be driven by data Address, 
*					and anodes 3-0 will be driven by data D_Out. 
*					data[31:16] = Address
*					data[15:0]  = D_Out
* 
* Notes: 		The default is set to send a hex zero. Since there is 32-bits of
*					data, then Reg_Out and Alu_Out output values of the register_file 
*					alongside the alu16 module must be concatenated into one wire 
*					32-bits wide in the TOP module. 
*****************************************************************************/
module ad_mux(sel, data, Y);

	input		[31:0]  data;
	input		[2:0]  sel; 
	output	[3:0]  Y; 
	reg 		[3:0]  Y; 
	
	always @(*) 
	case (sel) // makes the decision
			3'b000: Y = data[31:28];   // ANODE 7 HOT
			3'b001: Y = data[27:24];   // ANODE 6 HOT
			3'b010: Y = data[23:20];   // ANODE 5 HOT
			3'b011: Y = data[19:16];   // ANODE 4 HOT
			3'b100: Y = data[15:12];   // ANODE 3 HOT
			3'b101: Y = data[11:8];    // ANODE 2 HOT
			3'b110: Y = data[7:4];     // ANODE 1 HOT
			3'b111: Y = data[3:0];     // ANODE O HOT
		  default: Y = 4'h0;
	endcase 

endmodule


