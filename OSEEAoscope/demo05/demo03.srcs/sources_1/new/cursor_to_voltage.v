`timescale 1ns / 1ps

module cursor_to_voltage
    #(parameter VOLTAGE_BITS = 12,
                DISPLAY_Y_BITS = 12,
                SCALE_EXPONENT_BITS = 4,
                PIXELS_TIMES_250 = 18,
                PIXELS_TIMES_250_SCALED = 21,
                SCALE_FACTOR_SIZE = 10)
    (
    input clock,
    input signed [DISPLAY_Y_BITS-1:0] y,
    input [SCALE_EXPONENT_BITS-1:0] scaleExponent,  //scale=8 -> scaleExponent=3
    input [SCALE_FACTOR_SIZE-1:0] scale,
    output reg signed [VOLTAGE_BITS-1:0] voltage,
    output reg [VOLTAGE_BITS-1:0] voltageAbsoluteValue,
    output reg isNegative  //0 if positive 1 if negative
    );
    
    reg signed [PIXELS_TIMES_250-1:0] pixelsToVoltage;
    reg signed [PIXELS_TIMES_250_SCALED-1:0] pixelsToVoltageScaled;
    reg signed [PIXELS_TIMES_250_SCALED-1:0] pixelsToVoltageScaledDivHeight;
    
    always @(posedge clock) begin
     
        //cycle 0
        pixelsToVoltage <= y * 10'sd256;
        
        //cycle 1
        pixelsToVoltageScaled <= pixelsToVoltage <<< 3;  //multiply by 8 (DEFAULT_SCALE = 8)
        
        //cycle 2
        pixelsToVoltageScaledDivHeight <= pixelsToVoltageScaled >>> 10;
                                        
        //cycle 3
        voltage <= pixelsToVoltageScaledDivHeight >>> scaleExponent;
        
        //cycle 4
        isNegative <= (voltage < 0) ? 1'b1 : 1'b0;
        voltageAbsoluteValue <= (voltage > 0) ? voltage : ((~voltage) + 1);
    end
    
endmodule

