`timescale 1ns / 1ps
/****************************** C E C S  3 0 1 ******************************
* File Name:	display_controller.v
* Project:		Lab Project 7: CPU Execution Unit
* Designer: 	Chorro, Edward / Velazquez, Alec
* Email: 		edwardchorro@gmail.com / velazquez_alec@yahoo.com
* Rev. Date: 	April 24, 2018 
* 
* Purpose: 		The purpose of this module is to instantiate all four(4) modules
*					that display the outputs onto the 7 segment display in one given 
*					module; the display controller. The modules that make up the 
*					display controller are the pixel_clk, the pixel_controller, the 
*					ad_mux, and the hex_to_7seg module. 
* 
* Notes: 		By creating this module, it eliminates the number of wires that
*					must be utilized in any "Top" module when tying a project together,
*					which results in less chance to make any logical erros. Furthermore,
*					the Hierarchy in our projects looks orderly. All four modules listed
*					above are "hiding" within this one display-controller module. 
*****************************************************************************/
module display_controller(clk, reset, data_to_mux, anode, a, b, c, d, e, f, g, dp);

	input 	clk, reset; 
	input		[31:0] data_to_mux; 
	output	[7:0] anode; 
	output	a, b, c, d, e, f, g, dp; 
	
	// Wires 
	// pixel_clk to pixel_controller
	wire		w0; 
	
	// pixel_controller to ad_mux
	wire		[2:0] w1; 
	
	// ad_mux to hex_to_7seg
	wire		[3:0] w2; 
	
	// Instatiate pixel_clk module 
	pixel_clk				pixel_CLK(.clk_in(clk), 
	                                  .reset(reset), 
	                                  .clk_out(w0)); 
	
	// Instantiate pixel_controller module 
	pixel_controller		pixel_CON(.clk(w0), 
	                                  .reset(reset), 
	                                  .anode(anode), 
	                                  .seg_sel(w1),
	                                  .dp(dp)); 
	
	// Instantiate ad_mux module 
	ad_mux					ad_MUX(.sel(w1), 
	                               .data(data_to_mux), 
	                               .Y(w2)); 
	
	// Instantiate hex_to_7seg module 
	hex_to_7seg				hex_to_7SEG(.hex_in(w2), 
	                                    .a(a), 
	                                    .b(b), 
	                                    .c(c), 
	                                    .d(d), 
	                                    .e(e), 
	                                    .f(f), 
	                                    .g(g)); 


endmodule

