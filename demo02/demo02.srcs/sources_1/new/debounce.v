`timescale 1ns / 1ps
/****************************** C E C S  3 0 1 ******************************
* File Name:	one_shot.v
* Project:		Lab Project 5: Register Files 
* Designer: 	Chorro, Edward / Velazquez, Alec
* Email: 		edwardchorro@gmail.com / velazquez_alec@yahoo.com
* Rev. Date: 	March 22, 2018 
* 
* Purpose: 		The purpose of this module is to create a "one shot" output 
*					signal. The "one shot" is refering to only one incoming clock 
*					tick to be outputed once a given switch is "pushed". Futhermore,
*					when a button is "pushed", the button's output "bounces" for a 
*					period of time. This "bouncing" is due to the elctro-magnetic 
*					nature of the switch. In order to send the "one shot" output 
*					signal, the signal should not be "bouncing" but, be stable. 
*					This technique is known as switch debouncing, or waiting enough 
*					time necessary for stabalization of the switch. When the button
*					is pushed, there are 10 samples that are tested to see if they 
*					are at the desired level (e.g. logic 1) except for the first
*					sample. This is at the "always" procedural block. The block is 
*					testing to see if the samples are stablaized. If at any point 
*					,beyond the first sample, one of the samples is equal to zero 
*					then new samples are shifted in to test again. Additionally, 
*					these samples are coming at the positive edge of the clock. Once 
*					all the samples are at the desired level for the desired amount 
*					of time, the output signal is sent (D_out) via the assign 
*					statement. And we can interpret as the switch output
*					to be stabalized.   
* 
* Notes: 		A genral "ball-park" of the time it takes for a switch to to 
*					enter stabalization is 20ms.
*****************************************************************************/
module debounce (clk, reset, D_in, D_out);
	
	input clk, reset, D_in;
	output wire D_out;
	
	reg q9, q8, q7, q6, q5, q4, q3, q2, q1, q0;
	
	always @ (posedge clk or posedge reset)
		if (reset == 1'b1)
			{q9, q8, q7, q6, q5, q4, q3, q2, q1, q0} <= 10'b0;
		else begin
		// shift in the new sample that's on the D_in input
			q9 <= q8; q8 <= q7; q7 <= q6; q6 <= q5; q5 <= q4;
			q4 <= q3; q3 <= q2; q2 <= q1; q1 <= q0; q0 <= D_in;
		end
	
	// create the debounced, one_shot output pulse
	assign D_out = !q9 & q8 & q7 & q6 & q5 &
						 q4 & q3 & q2 & q1 & q0;
endmodule
						 
