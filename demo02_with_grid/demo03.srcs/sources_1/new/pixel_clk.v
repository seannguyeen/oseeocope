`timescale 1ns / 1ps
/****************************** C E C S  3 0 1 ******************************
* File Name:	pixel_clk.v
* Project:		Lab Project 7: CPU Execution Unit
* Designer: 	Chorro, Edward / Velazquez, Alec
* Email: 		edwardchorro@gmail.com / velazquez_alec@yahoo.com
* Rev. Date: 	April 24, 2018 
* 
* Purpose: 		The purpose of this module is to divide the clock to the desired 
*					frequency. The Nexys 4DDR board has a clock frequencey of 100MHz. 
*					For Lab Project 7, the desired clock requency for the pixel_clk
*					is 480Hz. The value of the counter that counts the incoming clock 
*					ticks is equal to 104167Hz. The 480Hz is used for refreshing the 
*					eight 7-segment displays. The output will feed the pixel_controller
*					clock input. 
* 
* Notes: 		The calculation for 100kHz is: 
*					[ (100M / 480) / 2] Insert result in if statement
*					Desired refresh rate is 60Hz.  
*****************************************************************************/
module pixel_clk(clk_in, reset, clk_out);

	input		clk_in, reset; 
	output	clk_out; 
	reg		clk_out; 
	integer 	i; 
	
	//********************************************************************
	// The following verilog code will "divide" an incoming clock 
	// by the 32-bit value specified in the "if condition" 
	// 
	// The value of the counter that counts the incoming clock ticks 
	// is equal to the [ (Incoming Freq / Outgoing Freq) / 2] 
	//********************************************************************
	
	always @(posedge clk_in or posedge reset) begin 
		if (reset == 1'b1) begin 
			i = 0; 
			clk_out = 0; 
		end 
		// got a clock, so increment the counter and 
		// test to see if half a period has elapsed 
		else begin 
			i = i + 1;
			if (i >= 104167) begin 
				clk_out = ~clk_out; 
				i = 0;
			end // if 
		end // else 
	end // always 

endmodule 



