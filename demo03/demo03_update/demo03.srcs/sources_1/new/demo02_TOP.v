`timescale 1ns / 1ps

module demo02(
   input clk,
   input rst,
   input addr_btn, 
   input addr_UHDL,
   input addr_sel,
   input we_sw,
   input vauxp11,
   input vauxn11,
   input vp_in,
   input vn_in,
   output [7:0] an,
   output dp,
   output a, b, c, d, e, f, g  
 );
   
   wire enable;  
   wire ready;
   wire [15:0] data;   
   reg [6:0] Address_in;     
   reg [32:0] decimal;   
   reg [3:0] dig0;
   reg [3:0] dig1;
   reg [3:0] dig2;
   reg [3:0] dig3;
   reg [3:0] dig4;
   reg [3:0] dig5;
   reg [3:0] dig6;
    // debounce
    wire addr_btn_wire; 
    
    debounce DB (.clk(clk), 
                 .reset(rst), 
                 .D_in(addr_btn), 
                 .D_out(addr_btn_wire)); 
                 
   //xadc instantiation connect the eoc_out .den_in to get continuous conversion
   xadc_wiz_0  XADC (.daddr_in(8'h1b), //addresses can be found in the artix 7 XADC user guide DRP register space
                     .dclk_in(clk), 
                     .den_in(enable), 
                     .di_in(0), 
                     .dwe_in(0), 
                     .busy_out(),                    
                     .vauxp2(),
                     .vauxn2(),
                     .vauxp3(),
                     .vauxn3(),
                     .vauxp10(),
                     .vauxn10(),
                     .vauxp11(vauxp11),
                     .vauxn11(vauxn11),
                     .vn_in(vn_in), 
                     .vp_in(vp_in), 
                     .alarm_out(), 
                     .do_out(data), 
                     .reset_in(0),
                     .eoc_out(enable),
                     .channel_out(),
                     .drdy_out(ready));
    
    reg [8:0] count_addr; 
    reg write_en; 
             
    // writing to all 512 address 
    always@(posedge clk, posedge rst)
        if(rst) 
            count_addr <= 9'b0; 
//            write_en <= 1'b0;  
        else 
            
            count_addr <= count_addr + 9'b1; 
//                write_en <= ready; 
               
//            else
//                count_addr <= count_addr; 
 
//                write_en <= 1'b0;
    
   
    // mux  
    reg [8:0] addr_toggle; 
    reg [8:0] read_addr; 
    
    // procedural to step with btn 
    always@(posedge clk, posedge rst) 
        if (rst) 
            read_addr <= 9'b0; 
        else
            case({addr_btn_wire, addr_UHDL}) 
                  2'b11: 
                        
                        read_addr <= read_addr + 9'b1;
                        
                  2'b10: 
                       
                        read_addr <= read_addr - 9'b1;
                        
                default: 
                       
                        read_addr <= read_addr;  
                       
            endcase 

//            if (addr_btn) 
//                if (addr_UHDL) 
//                    read_addr <= read_addr + 9'b1;  
//                else 
//                    read_addr <= read_addr - 9'b1;  
//            else 
//                read_addr <= read_addr;  

//    assign addr_toggle = read_addr ? (addr_sel == 1'b1) : count_addr ; 
    always@(*) 
        if (addr_sel == 1'b1) 
            addr_toggle = read_addr; 
        else 
            addr_toggle = count_addr; 
    
    always@(*) 
        if (we_sw == 1'b1) 
            write_en = ready; 
        else 
            write_en = 1'b0; 

    wire [15:0] mem_out; 
   
    blk_mem_gen_0 MEM (.clka(clk), 
                              .ena(1'b1), 
                              .wea(write_en), 
                              .addra(addr_toggle), 
                              .dina(data), 
                              .douta(mem_out)); 
   
     reg [32:0] count; 
     
     //binary to decimal conversion
      always @ (posedge(clk))
      begin
      
        if(count == 10000000)begin
        
        decimal = mem_out >> 4;
        //looks nicer if our max value is 1V instead of .999755
        if(decimal >= 4093)
        begin
            dig0 = 0;
            dig1 = 0;
            dig2 = 0;
            dig3 = 0;
            dig4 = 0;
            dig5 = 0;
            dig6 = 1;
            count = 0;
        end
        else 
        begin
            decimal = decimal * 250000;
            decimal = decimal >> 10;
            
            dig0 = decimal % 10;
            decimal = decimal / 10;
            
            dig1 = decimal % 10;
            decimal = decimal / 10;
                   
            dig2 = decimal % 10;
            decimal = decimal / 10;
            
            dig3 = decimal % 10;
            decimal = decimal / 10;
            
            dig4 = decimal % 10;
            decimal = decimal / 10;
                   
            dig5 = decimal % 10;
            decimal = decimal / 10; 
            
            dig6 = decimal % 10;
            decimal = decimal / 10; 
            
            count = 0;
        end
       end
      count = count + 1;
      end
      
      wire [3:0] hex0,
                 hex1,
                 hex2; 
      
      assign hex0 = addr_toggle[3:0]; 
      assign hex1 = addr_toggle[7:4]; 
      assign hex2 = {3'b0, addr_toggle[8]}; 

    wire [31:0] data2_seg; 
 
    assign data2_seg = {4'h0, hex2, hex1, hex0, dig6, dig5, dig4, dig3}; 
      
      display_controller DC (.clk(clk), 
                             .reset(rst), 
                             .data_to_mux(data2_seg), 
                             .anode(an), 
                             .a(a), 
                             .b(b), 
                             .c(c), 
                             .d(d), 
                             .e(e), 
                             .f(f), 
                             .g(g),
                             .dp(dp)); 
                             
//      DigitToSeg SEG_DISP   (.in1(dig3),
//                         .in2(dig4),
//                         .in3(dig5),
//                         .in4(dig6),
//                         .in5(hex0),
//                         .in6(hex1),
//                         .in7(hex2),
//                         .in8(),
//                         .mclk(clk),
//                         .an(an),
//                         .dp(dp),
//                         .seg(seg));  
endmodule
