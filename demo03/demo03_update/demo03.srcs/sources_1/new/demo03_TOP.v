`timescale 1ns / 1ps

module demo03_TOP(
   input clk,
   input rst,
   input addr_btn, 
   input addr_UHDL,
   input addr_sel,
   input we_sw,
   input vauxp11,
   input vauxn11,
   input vp_in,
   input vn_in,
   output [7:0] an,
   output dp,
   output a, b, c, d, e, f, g,  
   output reg VGA_HS,
   output reg VGA_VS,
   output reg [3:0] VGA_R, VGA_G, VGA_B);
    
   // PARAMETER DEF
    parameter ADDRESS_BITS = 12;
    parameter   REAL_DISPLAY_WIDTH = 1688;
    parameter REAL_DISPLAY_HEIGHT = 1066; 
    parameter SAMPLE_BITS = 12;
    parameter TOGGLE_CHANNELS_STATE_BITS = 2;
    parameter DISPLAY_X_BITS = 12;
    parameter DISPLAY_Y_BITS = 12;
    parameter  RGB_BITS = 12;   
  
    // CLK WIZ 
    wire CLK108MHZ;
    wire locked;
 
    clk_wiz_0 clk_wiz_vga (.clk_in1(clk),      
                           .clk_out1(CLK108MHZ),    
                           .reset(0),
                           .locked(locked));      
                           
    // XADC                       
    wire ready;
    wire [15:0] xadc_dataOut;   
    wire  [SAMPLE_BITS-1:0] channel1;
    wire  [SAMPLE_BITS-1:0] channel2;
    wire channelDataReady;
    wire [TOGGLE_CHANNELS_STATE_BITS-1:0] state;
    wire [TOGGLE_CHANNELS_STATE_BITS-1:0] previousState;
    wire endOfConversion,
         DRPEnable,
         DRPWriteEnable;
    wire [7:0] DRPAddress;  
            
    xadc       XADC (.daddr_in(DRPAddress), //addresses can be found in the artix 7 XADC user guide DRP register space
                     .dclk_in(CLK108MHZ), 
                     .den_in(DRPEnable), 
                     .di_in(0), 
                     .dwe_in(DRPWriteEnable), 
                     .busy_out(),                    
                     .vauxp3(),
                     .vauxn3(),
                     .vauxp11(vauxp11),
                     .vauxn11(vauxn11),
                     .vn_in(vn_in), 
                     .vp_in(vp_in), 
                     .alarm_out(), 
                     .do_out(xadc_dataOut), 
                     .reset_in(0),
                     .eoc_out(endOfConversion),
                     .channel_out(),
                     .drdy_out(ready));
                     
    // TOGGLE SELECTION (RAW VS BUFFER)
    toggle_channel TOGGLE_CHA(.clock(CLK108MHZ),
                              .endOfConversion(endOfConversion),
                              .DRPReady(ready),
                              .DRPDataOut(xadc_dataOut[15:4]),
                              .DRPEnable(DRPEnable),
                              .DRPWriteEnable(DRPWriteEnable),
                              .channel1(channel1),
                              .channel2(), //will use once we get ch1 working 
                              .DRPAddress(DRPAddress),
                              .channelDataReady(channelDataReady),
                              .state(state),
                              .previousState(previousState));
    
    // ADC CONTROLLER
    wire adcc_ready;
    wire adccRawReady;
    wire signed [11:0] ADCCdataOutChannel1;
    wire signed [11:0] adccRawDataOutChannel1;
    wire signed [11:0] ADCCdataOutChannel2;
    wire signed [11:0] adccRawDataOutChannel2;
    
    adc_controller ADCC(.clock(CLK108MHZ),
                        .reset(rst),
                        .sampleEnabled(1),
                        .inputReady(channelDataReady),
                        .samplePeriod(0), // to be added on later with settings 
                        .ready(adcc_ready),
                        .rawReady(adccRawReady), // not affected by samplePeriod
                        .dataInChannel1(channel1),
                        .dataOutChannel1(ADCCdataOutChannel1),
                        .rawDataOutChannel1(adccRawDataOutChannel1),
                        .dataInChannel2(0), // no ch2 yet
                        .dataOutChannel2(), // no ch2 yet
                        .rawDataOutChannel2()); // no ch2 yet
    
    // LOW PASS FILTER
    wire risingEdgeReadyChannel1;
    wire signed [13:0] slopeChannel1;
    wire positiveSlopeChannel1;
    low_pass_filter FILTER(.clock(CLK108MHZ),
                           .dataReady(adccRawReady),
                           .dataIn(adccRawDataOutChannel1),
                           .risingEdgeReady(risingEdgeReadyChannel1),
                           .estimatedSlope(slopeChannel1),
                           .estimatedSlopeIsPositive(positiveSlopeChannel1));
                           
    // BUFFER SELECTION
    wire [ADDRESS_BITS-1:0] curveAddressOutChannel1;
    wire [ADDRESS_BITS-1:0] curveAddressOutChannel2;
    wire isTriggered;
    wire [11:0] bufferDataOutChannel1;
    wire drawStarting; 
    wire activeBramSelect;
    
    buffer_select BUFF_SEL (.clock(CLK108MHZ),
                            .drawStarting(drawStarting),
                            .activeBramSelect(activeBramSelect));
        
    buffer BufferChannel (.clock(CLK108MHZ), 
                          .ready(ready), 
                          .dataIn(xadc_dataOut[15:4]), //grabbing upper 12 bits 
                          //.isTrigger(isTriggered), 
                          .disableCollection(0), 
                          .activeBramSelect(activeBramSelect),
                          .reset(rst),
                          .readTriggerRelative(1),
                          .readAddress(curveAddressOutChannel1),
                          .dataOut(bufferDataOutChannel1));
                        
    wire [11:0]  buffer2DataOutChannel1;
    wire [ADDRESS_BITS-1:0] curveAddressOut2Channel1; 
    
    buffer Buffer2Channel (.clock(CLK108MHZ), 
                           .ready(risingEdgeReadyChannel1), 
                           .dataIn(slopeChannel1),
                           //.isTrigger(isTriggered), 
                           .disableCollection(0), 
                           .activeBramSelect(activeBramSelect),
                           .reset(rst),
                           .readTriggerRelative(1),
                           .readAddress(curveAddressOut2Channel1),
                           .dataOut(buffer2DataOutChannel1));
    
    // VGA                                    
    wire [11:0] displayX;
    wire [11:0] displayY;
    wire [3:0] vga_r;
    wire [3:0] vga_g;
    wire [3:0] vga_b;
    wire vsync;
    wire hsync;
    wire blank;

    xvga1280_1024 x(.vclock(CLK108MHZ),
                    .displayX(displayX), // pixel number on current line
                    .displayY(displayY), // line number
                    .vsync(vsync),
                    .hsync(hsync),
                    .blank(blank));
   
   // GRID 
    wire [11:0] gridDisplayX,
                gridDisplayY, 
                gridPixel; 
    wire gridHsync, 
         gridVsync,
         gridBlank; 
        
    Grid GRID(.clock(CLK108MHZ), 
                .displayX(displayX), 
                .displayY(displayY), 
                .hsync(hsync), 
                .vsync(vsync), 
                .blank(blank),
                .gridDisplayX(gridDisplayX), 
                .gridDisplayY(gridDisplayY), 
                .gridHsync(gridHsync), 
                .gridVsync(gridVsync), 
                .gridBlank(gridBlank), 
                .pixel(gridPixel));
    
    // DRAW WAVE
    wire [11:0] dataInChannel1;
    wire [DISPLAY_X_BITS-1:0] curveChannel1DisplayX;
    wire [DISPLAY_Y_BITS-1:0] curveChannel1DisplayY;
    wire curveChannel1Hsync;
    wire curveChannel1Vsync;
    wire curveChannel1Blank;
    wire [RGB_BITS-1:0] curveChannel1Pixel;
    

    assign dataInChannel1 = bufferDataOutChannel1;
    draw_wave #(.ADDRESS_BITS(ADDRESS_BITS))
            DRAW_WAVE_CH1
            (.clock(CLK108MHZ),
            .dataIn(dataInChannel1),
            .verticalScaleFactorTimes8(1), //Hard code (comes from user settings) 
            .displayX(gridDisplayX),
            .displayY(gridDisplayY),
            .hsync(gridHsync),
            .vsync(gridVsync),
            .blank(gridBlank),
            .previousPixel(gridPixel),
            .pixel(curveChannel1Pixel),
            .drawStarting(),
            .address(curveAddressOutChannel1),
            .curveDisplayX(curveChannel1DisplayX),
            .curveDisplayY(curveChannel1DisplayY),
            .curveHsync(curveChannel1Hsync),
            .curveVsync(curveChannel1Vsync),
            .curveBlank(curveChannel1Blank)
            );    

    // DRAWSTRING SIGNAL (END OF VGA SCAN) 
    assign drawStarting = (gridDisplayX == REAL_DISPLAY_WIDTH - 1) && (gridDisplayY == (REAL_DISPLAY_HEIGHT - 1));
    
    // ASSIGN FINAL VGA PIXELS 
    always @(posedge CLK108MHZ) 
        begin
        {VGA_R, VGA_G, VGA_B} <= !curveChannel1Blank ? curveChannel1Pixel : 12'b0;
        VGA_HS <= curveChannel1Hsync;
        VGA_VS <= curveChannel1Vsync;
        end   
         
endmodule
