`timescale 1ns / 1ps
/****************************** C E C S  3 0 1 ******************************
* File Name:	hex_to_7seg.v
* Project:		Lab Project 7: CPU Execution Unit
* Designer: 	Chorro, Edward / Velazquez, Alec
* Email: 		edwardchorro@gmail.com / velazquez_alec@yahoo.com
* Rev. Date: 	April 24, 2018 
* 
* Purpose: 		The purpose of this decoder module is to take the incoming data,
*					which is always going to be 4-bits wide, and assigns which LEDS
*					to be turned on in the 7-segment display based upon the nibble
*					(4-bits) value. Notice that now all hexadecimal values will be 
*					displayed on the 7-segment display (0-F). Hexadecimal valued B &
*					D are lowercase. The ouputs are the seven segments represented 
*					by {a, b, c, d, e, f, g}. 
* 
* Notes: 		In the case statment, when a given segment is equal to one then 
*					the LED will be turned off. When the segment is equal to zero, 
*					then the LED will be turned on. Default is set to have none of the
*					LED's to be turned on.  
*****************************************************************************/
module hex_to_7seg( hex_in, a, b, c, d, e, f, g);

	input		[3:0] hex_in; 
	output 	a, b, c, d, e, f, g;
	reg 		a, b, c, d, e, f, g;  
	
	always @(*)
		case(hex_in) 											    // VALUE
				4'h0 : {a, b, c, d, e, f, g} = 7'b0000001; // 0
				4'h1 : {a, b, c, d, e, f, g} = 7'b1001111; // 1
				4'h2 : {a, b, c, d, e, f, g} = 7'b0010010; // 2
				4'h3 : {a, b, c, d, e, f, g} = 7'b0000110; // 3
				4'h4 : {a, b, c, d, e, f, g} = 7'b1001100; // 4
				4'h5 : {a, b, c, d, e, f, g} = 7'b0100100; // 5
				4'h6 : {a, b, c, d, e, f, g} = 7'b0100000; // 6
				4'h7 : {a, b, c, d, e, f, g} = 7'b0001111; // 7
				4'h8 : {a, b, c, d, e, f, g} = 7'b0000000; // 8
				4'h9 : {a, b, c, d, e, f, g} = 7'b0001100; // 9
				4'hA : {a, b, c, d, e, f, g} = 7'b0001000; // A
				4'hB : {a, b, c, d, e, f, g} = 7'b1100000; // b
				4'hC : {a, b, c, d, e, f, g} = 7'b0110001; // C
				4'hD : {a, b, c, d, e, f, g} = 7'b1000010; // d
				4'hE : {a, b, c, d, e, f, g} = 7'b0110000; // E
				4'hF : {a, b, c, d, e, f, g} = 7'b0111000; // F
			default : {a, b, c, d, e, f, g} = 7'b1111110; // DASH
		endcase 
	
endmodule



