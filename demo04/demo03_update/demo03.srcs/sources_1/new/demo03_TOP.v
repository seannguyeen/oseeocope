`timescale 1ns / 1ps

module demo03_TOP(
   input clk,
   input reset,
   input up_btn, 
   input dn_btn,
   input autoset_btn,
   input [4:0] SW,
   input vauxp11,
   input vauxn11,
   input vp_in,
   input vn_in,
    
   output reg VGA_HS,
   output reg VGA_VS,
   output reg [3:0] VGA_R, VGA_G, VGA_B);
    
   // PARAMETER DEF
    parameter ADDRESS_BITS = 12;
    parameter   REAL_DISPLAY_WIDTH = 1688;
    parameter REAL_DISPLAY_HEIGHT = 1066; 
    parameter SAMPLE_BITS = 12;
    parameter TOGGLE_CHANNELS_STATE_BITS = 2;
    parameter DISPLAY_X_BITS = 12;
    parameter DISPLAY_Y_BITS = 12;
    parameter  RGB_BITS = 12;   
    parameter SCALE_EXPONENT_BITS = 4; 
    parameter SCALE_FACTOR_BITS = 10;
  
    
    // CLK WIZ 
    wire CLK108MHZ;
    wire locked;
 
    clk_wiz_0 clk_wiz_vga (.clk_in1(clk),      
                           .clk_out1(CLK108MHZ),    
                           .reset(0),
                           .locked(locked));      
    
    // DEBOUNCE ALL MODULES 
    wire rst, db_up, db_dn, db_autoset;
    debounce CENTER(.clk(CLK108MHZ), 
                    .reset(reset),
                    .D_in(reset), 
                    .D_out(rst)), 
                                      
             UP (.clk(CLK108MHZ), 
                 .reset(reset),
                 .D_in(up_btn), 
                 .D_out(db_up)),
             
             DOWN (.clk(CLK108MHZ), 
                   .reset(reset),
                   .D_in(dn_btn), 
                   .D_out(db_dn)),
                   
            AUTO (.clk(CLK108MHZ), 
                  .reset(reset),
                  .D_in(autoset_btn), 
                  .D_out(db_autoset));
   
    // SETTINGS
    wire  signed [11:0] triggerThreshold;
    wire [9:0] verticalScaleFactorTimes8Channel1;
    wire [9:0] verticalScaleFactorTimes8Channel2;
    wire [5:0] samplePeriod;
    wire channelSelected;
    wire xyDisplayMode;
    wire  [DISPLAY_Y_BITS-1:0] yCursor1;
    wire  [DISPLAY_Y_BITS-1:0] yCursor2;
    
    // measure_signal wires 
    wire  [11:0] signalMinChannel1;
    wire  [11:0] signalMaxChannel1;
    wire [11:0] signalPeriodChannel1;
    wire  [11:0] signalAverageChannel1;
    wire  [11:0] signalMinChannel2;
    wire  [11:0] signalMaxChannel2;
    wire [11:0] signalPeriodChannel2;
    wire  [11:0] signalAverageChannel2;
    
    settings SETTINGS(.clock(CLK108MHZ), 
                      .sw(SW),
                      .btnu(db_up), 
                      .btnd(db_dn), 
                      .btnc(rst), 
                      .btnl(db_autoset),
                      .buttonUpClean(up_btn), 
                      .buttonDownClean(dn_btn),
                      .signalMinChannel1(signalMinChannel1), 
                      .signalMaxChannel1(signalMaxChannel1), 
                      .signalPeriodChannel1(signalPeriodChannel1),
                      .signalMinChannel2(signalMinChannel2), 
                      .signalMaxChannel2(signalMaxChannel2), 
                      .signalPeriodChannel2(signalPeriodChannel2),
                      .triggerThreshold(triggerThreshold), 
                      .verticalScaleFactorTimes8Channel1(verticalScaleFactorTimes8Channel1), 
                      .verticalScaleFactorTimes8Channel2(verticalScaleFactorTimes8Channel2),
                      .samplePeriod(samplePeriod), 
                      .channelSelected(channelSelected),
                      .yCursor1(yCursor1),
                      .yCursor2(yCursor2));
    
    // VERTICAL SCALE CONV
    wire [3:0] verticalScaleExponentChannel1;
    wire [3:0] verticalScaleExponentChannel2;
    vertScale_conversion VERT_SCALE (.clock(CLK108MHZ),
                                     .verticalScaleFactorTimes8Channel1(verticalScaleFactorTimes8Channel1),
                                     .verticalScaleFactorTimes8Channel2(verticalScaleFactorTimes8Channel2),
                                     .verticalScaleExponentChannel1(verticalScaleExponentChannel1),
                                     .verticalScaleExponentChannel2(verticalScaleExponentChannel2));
    
    // XADC     
    wire ready;
    wire [15:0] xadc_dataOut;   
    wire  [SAMPLE_BITS-1:0] channel1;
    wire  [SAMPLE_BITS-1:0] channel2;
    wire channelDataReady;
    wire [TOGGLE_CHANNELS_STATE_BITS-1:0] state;
    wire [TOGGLE_CHANNELS_STATE_BITS-1:0] previousState;
    wire endOfConversion,
         DRPEnable,
         DRPWriteEnable;
    wire [7:0] DRPAddress;  
            
    xadc       XADC (.daddr_in(DRPAddress), //addresses can be found in the artix 7 XADC user guide DRP register space
                     .dclk_in(CLK108MHZ), 
                     .den_in(DRPEnable), 
                     .di_in(0), 
                     .dwe_in(DRPWriteEnable), 
                     .busy_out(),                    
                     .vauxp3(),
                     .vauxn3(),
                     .vauxp11(vauxp11),
                     .vauxn11(vauxn11),
                     .vn_in(vn_in), 
                     .vp_in(vp_in), 
                     .alarm_out(), 
                     .do_out(xadc_dataOut), 
                     .reset_in(0),
                     .eoc_out(endOfConversion),
                     .channel_out(),
                     .drdy_out(ready));
                     
    // TOGGLE SELECTION (RAW VS BUFFER)
    toggle_channel TOGGLE_CHA(.clock(CLK108MHZ),
                              .endOfConversion(endOfConversion),
                              .DRPReady(ready),
                              .DRPDataOut(xadc_dataOut[15:4]),
                              .DRPEnable(DRPEnable),
                              .DRPWriteEnable(DRPWriteEnable),
                              .channel1(channel1),
                              .channel2(), //will use once we get ch1 working 
                              .DRPAddress(DRPAddress),
                              .channelDataReady(channelDataReady),
                              .state(state),
                              .previousState(previousState));
    
    // ADC CONTROLLER
    wire adcc_ready;
    wire adccRawReady;
    wire  [11:0] ADCCdataOutChannel1;
    wire  [11:0] adccRawDataOutChannel1;
    wire  [11:0] ADCCdataOutChannel2;
    wire  [11:0] adccRawDataOutChannel2;
    
    adc_controller ADCC(.clock(CLK108MHZ),
                        .reset(rst),
                        .sampleEnabled(1),
                        .inputReady(channelDataReady),
                        .samplePeriod(samplePeriod), // to be added on later with settings 
                        .ready(adcc_ready),
                        .rawReady(adccRawReady), // not affected by samplePeriod
                        .dataInChannel1(channel1),
                        .dataOutChannel1(ADCCdataOutChannel1),
                        .rawDataOutChannel1(adccRawDataOutChannel1),
                        .dataInChannel2(0), // no ch2 yet
                        .dataOutChannel2(), // no ch2 yet
                        .rawDataOutChannel2()); // no ch2 yet
    
    // LOW PASS FILTER
    wire risingEdgeReadyChannel1;
    wire  [13:0] slopeChannel1;
    wire positiveSlopeChannel1;
    low_pass_filter FILTER(.clock(CLK108MHZ),
                           .dataReady(adccRawReady),
                           .dataIn(adccRawDataOutChannel1),
                           .risingEdgeReady(risingEdgeReadyChannel1),
                           .estimatedSlope(slopeChannel1),
                           .estimatedSlopeIsPositive(positiveSlopeChannel1));
                           
    // BUFFER SELECTION
    wire [ADDRESS_BITS-1:0] curveAddressOutChannel1;
    wire [ADDRESS_BITS-1:0] curveAddressOutChannel2;
    wire isTriggered;
    wire [11:0] bufferDataOutChannel1;
    wire drawStarting; 
    wire activeBramSelect;
    
    buffer_select BUFF_SEL (.clock(CLK108MHZ),
                            .drawStarting(drawStarting),
                            .activeBramSelect(activeBramSelect));
        
    buffer BUFF_CH1 (.clock(CLK108MHZ), 
                     .ready(ready), 
                     .dataIn(xadc_dataOut[15:4]), //grabbing upper 12 bits 
                     .isTrigger(isTriggered), 
                     .disableCollection(0), 
                     .activeBramSelect(activeBramSelect),
                     .reset(rst),
                     .readTriggerRelative(1),
                     .readAddress(curveAddressOutChannel1),
                     .dataOut(bufferDataOutChannel1));
                        
    wire [11:0]  buffer2DataOutChannel1;
    wire [ADDRESS_BITS-1:0] curveAddressOut2Channel1; 
    
    buffer BUFF_CH2 (.clock(CLK108MHZ), 
                     .ready(risingEdgeReadyChannel1), 
                     .dataIn(slopeChannel1),
                     .isTrigger(isTriggered), 
                     .disableCollection(0), 
                     .activeBramSelect(activeBramSelect),
                     .reset(rst),
                     .readTriggerRelative(1),
                     .readAddress(curveAddressOut2Channel1),
                     .dataOut(buffer2DataOutChannel1));
    
    // SOFTWARE TRIGGER 
    wire [SAMPLE_BITS-1:0] channelSelectedData;
    wire positiveSlopeChannelSelected;
    wire [SCALE_FACTOR_BITS-1:0] verticalScaleFactorTimes8ChannelSelected;
    wire [SCALE_EXPONENT_BITS-1:0] verticalScaleExponentChannelSelected;
    
    channel_toggle CH_TOGGLE
                (.clock(CLK108MHZ),
                .channel1(adccRawDataOutChannel1),
                .channel2(adccRawDataOutChannel2),
                .positiveSlopeChannel1(positiveSlopeChannel1),
                .positiveSlopeChannel2(0),
                .verticalScaleFactorTimes8Channel1(verticalScaleFactorTimes8Channel1),
                .verticalScaleFactorTimes8Channel2(0),
                .verticalScaleExponentChannel1(verticalScaleExponentChannel1),
                .verticalScaleExponentChannel2(0),
                .channelSelected(channelSelected),
                .channelSelectedData(channelSelectedData),
                .positiveSlopeChannelSelected(positiveSlopeChannelSelected),
                .verticalScaleFactorTimes8ChannelSelected(verticalScaleFactorTimes8ChannelSelected),
                .verticalScaleExponentChannelSelected(verticalScaleExponentChannelSelected)
                );
            
        sw_trigger #(.DATA_BITS(12))
                TRIGGER
                (.clock(CLK108MHZ),
                .threshold(triggerThreshold),
                .dataReady(adccRawReady),
                .dataIn(channelSelectedData),
                .triggerDisable(~positiveSlopeChannelSelected),
                .isTriggered(isTriggered)
                );
    
    // MEASURE SIGNAL 
    measure_signal MEASURE_SIGNAL_CH1(.clock(CLK108MHZ),
                                      .dataReady(adccRawReady),
                                      .dataIn(adccRawDataOutChannel1),
                                      .isTrigger(isTriggered),
                                      .signalMax(signalMaxChannel1),
                                      .signalMin(signalMinChannel1),
                                      .signalPeriod(signalPeriodChannel1),
                                      .signalAverage(signalAverageChannel1));
      
    // VGA                                    
    wire [11:0] displayX;
    wire [11:0] displayY;
    wire [3:0] vga_r;
    wire [3:0] vga_g;
    wire [3:0] vga_b;
    wire vsync;
    wire hsync;
    wire blank;

    xvga1280_1024 VGA_CONTRL(.vclock(CLK108MHZ),
                             .displayX(displayX), // pixel number on current line
                             .displayY(displayY), // line number
                             .vsync(vsync),
                             .hsync(hsync),
                             .blank(blank));
   
   // GRID 
    wire [11:0] gridDisplayX,
                gridDisplayY, 
                gridPixel; 
    wire gridHsync, 
         gridVsync,
         gridBlank; 
        
    Grid GRID(.clock(CLK108MHZ), 
                .displayX(displayX), 
                .displayY(displayY), 
                .hsync(hsync), 
                .vsync(vsync), 
                .blank(blank),
                .gridDisplayX(gridDisplayX), 
                .gridDisplayY(gridDisplayY), 
                .gridHsync(gridHsync), 
                .gridVsync(gridVsync), 
                .gridBlank(gridBlank), 
                .pixel(gridPixel));
    
    // DRAW WAVE
    wire [11:0] dataInChannel1;
    wire [DISPLAY_X_BITS-1:0] curveChannel1DisplayX;
    wire [DISPLAY_Y_BITS-1:0] curveChannel1DisplayY;
    wire curveChannel1Hsync;
    wire curveChannel1Vsync;
    wire curveChannel1Blank;
    wire [RGB_BITS-1:0] curveChannel1Pixel;
    

    assign dataInChannel1 = bufferDataOutChannel1;
    draw_wave #(.ADDRESS_BITS(ADDRESS_BITS))
            DRAW_WAVE_CH1
            (.clock(CLK108MHZ),
            .dataIn(dataInChannel1),
            .verticalScaleFactorTimes8(verticalScaleFactorTimes8Channel1), //Hard code (comes from user settings) 
            .displayX(gridDisplayX),
            .displayY(gridDisplayY),
            .hsync(gridHsync),
            .vsync(gridVsync),
            .blank(gridBlank),
            .previousPixel(gridPixel),
            .pixel(curveChannel1Pixel),
            .drawStarting(), // Not ties to any logic 
            .address(curveAddressOutChannel1),
            .curveDisplayX(curveChannel1DisplayX),
            .curveDisplayY(curveChannel1DisplayY),
            .curveHsync(curveChannel1Hsync),
            .curveVsync(curveChannel1Vsync),
            .curveBlank(curveChannel1Blank)
            );    

    // DRAWSTRING SIGNAL (END OF VGA SCAN) 
    assign drawStarting = (gridDisplayX == REAL_DISPLAY_WIDTH - 1) && (gridDisplayY == (REAL_DISPLAY_HEIGHT - 1));
    
    // DRAW TRIGGER HORIZONTAL LINE
    wire [RGB_BITS-1:0] tlsPixel;
    wire [DISPLAY_X_BITS-1:0] tlsDisplayX;
    wire [DISPLAY_Y_BITS-1:0] tlsDisplayY;
    wire tlsHsync, tlsVsync, tlsBlank;
    
    draw_horizontal_line DRAW_TRIGGER (.clock(CLK108MHZ),
                                       .level(triggerThreshold * $signed(verticalScaleFactorTimes8ChannelSelected) / 'sd8),
                                       .displayX(curveChannel1DisplayX),
                                       .displayY(curveChannel1DisplayY),
                                       .hsync(curveChannel1Hsync),
                                       .vsync(curveChannel1Vsync),
                                       .blank(curveChannel1Blank),
                                       .previousPixel(curveChannel1Pixel),
                                       .pixel(tlsPixel),
                                       .spriteDisplayX(tlsDisplayX),
                                       .spriteDisplayY(tlsDisplayY),
                                       .spriteHsync(tlsHsync),
                                       .spriteVsync(tlsVsync),
                                       .spriteBlank(tlsBlank)); 
               
    // ASSIGN FINAL VGA PIXELS 
    always @(posedge CLK108MHZ) 
        begin
        {VGA_R, VGA_G, VGA_B} <= !tlsBlank?  tlsPixel: 12'b0;
        VGA_HS <= tlsHsync;
        VGA_VS <= tlsVsync;
        end   
         
endmodule
