`timescale 1ns / 1ps

module vertScale_conversion
    #(parameter SCALE_FACTOR_SIZE = 10,
                SCALE_EXPONENT_BITS = 4)
    (input clock,
    input [SCALE_FACTOR_SIZE-1:0] verticalScaleFactorTimes8Channel1,
    input [SCALE_FACTOR_SIZE-1:0] verticalScaleFactorTimes8Channel2,
    output reg [SCALE_EXPONENT_BITS-1:0] verticalScaleExponentChannel1,
    output reg [SCALE_EXPONENT_BITS-1:0] verticalScaleExponentChannel2
    );
    
    always @ (posedge clock) begin
        //channel 1
        case(verticalScaleFactorTimes8Channel1)  
            10'd1    : verticalScaleExponentChannel1 <= 4'd0;
            10'd2    : verticalScaleExponentChannel1 <= 4'd1; 
            10'd4    : verticalScaleExponentChannel1 <= 4'd2; 
            10'd8    : verticalScaleExponentChannel1 <= 4'd3; 
            10'd16   : verticalScaleExponentChannel1 <= 4'd4; 
            10'd32   : verticalScaleExponentChannel1 <= 4'd5; 
            10'd64   : verticalScaleExponentChannel1 <= 4'd6; 
            10'd128  : verticalScaleExponentChannel1 <= 4'd7; 
            10'd256  : verticalScaleExponentChannel1 <= 4'd8; 
            10'd512  : verticalScaleExponentChannel1 <= 4'd9; 
            10'd1024 : verticalScaleExponentChannel1 <= 4'd10;
        endcase      
        
        //channel 2
        case(verticalScaleFactorTimes8Channel2)  
            10'd1    : verticalScaleExponentChannel2 <= 4'd0;
            10'd2    : verticalScaleExponentChannel2 <= 4'd1; 
            10'd4    : verticalScaleExponentChannel2 <= 4'd2; 
            10'd8    : verticalScaleExponentChannel2 <= 4'd3; 
            10'd16   : verticalScaleExponentChannel2 <= 4'd4; 
            10'd32   : verticalScaleExponentChannel2 <= 4'd5; 
            10'd64   : verticalScaleExponentChannel2 <= 4'd6; 
            10'd128  : verticalScaleExponentChannel2 <= 4'd7; 
            10'd256  : verticalScaleExponentChannel2 <= 4'd8; 
            10'd512  : verticalScaleExponentChannel2 <= 4'd9; 
            10'd1024 : verticalScaleExponentChannel2 <= 4'd10;
        endcase          
    end   
    
endmodule
