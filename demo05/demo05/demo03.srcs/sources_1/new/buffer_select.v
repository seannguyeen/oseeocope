`timescale 1ns / 1ps

module buffer_select(

    input clock,
    input drawStarting,
    output reg activeBramSelect
    );
    
    always @(posedge clock) begin
        if (drawStarting)
            activeBramSelect <= ~activeBramSelect;
    end
endmodule
