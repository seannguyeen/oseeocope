`timescale 1ns / 1ps

module one_shot(
    input clk,
    input btn,
    output reg btnpulse
    );
    
    reg pulsedon = 0;
    
    always @(posedge clk) begin
        if (!pulsedon && btn) begin
            btnpulse <= 1;
            pulsedon <= 1;
        end else begin
            btnpulse <= 0;
            
            if (!btn)
                pulsedon <= 0;
        end
    end
endmodule